BDD UI testing for the Platform using Selenium + Behave in Python.

Everything within this repo now runs via Sauce Labs VMs.

Documentation:
https://dragosinc.atlassian.net/wiki/spaces/EN/pages/1338606040/Dragos+QA+-+Platform+UI+Testing
