#!/usr/bin/env python3

'''
Dragos Behave Environment
'''

import json
import os
import sys
from subprocess import check_output
import urllib3
from selenium import webdriver
from behave import use_step_matcher


use_step_matcher("re")
urllib3.disable_warnings()
SL_HUB = ('https://ondemand.saucelabs.com:443/wd/hub')


def sauce_proxy_check(environment_dict):
    '''
    Verify Sauce Labs Proxy is running.
    '''
    cmd = (f"ps -ef | grep {environment_dict['username']} | "
           f"grep {environment_dict['tunnelIdentifier']} "
           " | awk '{ print$2 }'")
    process = check_output(cmd, shell=True).decode('utf-8').split()
    if len(process) <= 1:
        raise IndexError
    return True


def before_scenario(context, feature):
    ''' Instantiate webdriver before each feature. '''
    capabilities = CONFIG['browsers'][TASK_ID]

    # Add the local/Jenkins environment variables
    capabilities.update(ENV)
    print('\nINFO:', capabilities)

    scenario = {}
    try:
        scenario['name'] = context.scenario.name
        print('Behave Scenario Name:', context.scenario.name)
        capabilities.update(scenario)
    except AttributeError:
        print('ERROR: Scenario name not found.')
        sys.exit(1)

    context.browser = webdriver.Remote(command_executor=SL_HUB,
                                       desired_capabilities=capabilities)

    # Required for Sauce Labs/Jenkins reporting
    session_id = context.browser.session_id
    print(f'SauceOnDemandSessionID={session_id} job-name={ENV["build"]}')


def after_scenario(context, feature):
    ''' Quit webdriver instance after the feature. '''
    context.browser.quit()


try:
    BROWSERS = os.environ['CONFIG_FILE']
    TASK_ID = int(os.environ['TASK_ID'])
    RUN_MODE = os.environ['RUN_MODE']
except NameError:
    print('This script should not be run stand-alone, but by '
          'behave_runner.py.  Browser configs (.json) and task ID '
          'should be passed as environment variables.\n\n'
          'Example: CONFIG_FILE=browsers.json TASK_ID=0 RUN_MODE=local '
          'behave features/single.feature')
    sys.exit(1)


ENV = {}
try:
    ENV['build'] = os.environ['SAUCE_BUILD_NAME']
    ENV['username'] = os.environ['SAUCE_USERNAME']
    ENV['accessKey'] = os.environ['SAUCE_ACCESS_KEY']
    ENV['tunnelIdentifier'] = os.environ['TUNNEL_IDENTIFIER']
except KeyError:
    print('ERROR: Missing required environment variables!\n\n'
          'Help:\nTry sourcing your local environment file.\n'
          'If Jenkins, debug using the sauce_labs_job_env.txt artifact.')
    sys.exit(1)


if RUN_MODE.lower() == 'local':
    try:
        sauce_proxy_check(ENV)
        print('INFO: Sauce Labs Proxy is running.')
    except IndexError:
        print('ERROR: Sauce Labs Proxy is not running.\n\n'
              'Run the start_sauce_proxy script and re-try.\n'
              'utils/sauce_connect/macos|linux/start_sauce_proxy.sh')
        sys.exit(1)


with open(BROWSERS, 'r') as data_file:
    CONFIG = json.load(data_file)
