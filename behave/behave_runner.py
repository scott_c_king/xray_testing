#!/usr/bin/env python3

'''
BrowserStack Behave Runner
'''

__author__ = "Scott C. King"
__version__ = "0.1.0"
__license__ = "MIT"

import json
import sys
import subprocess


def build_log_filename(config_dictionary):
    '''
    Return a valid log filename based on the instance config.
    '''
    platform = config_dictionary['platform']
    browser_name = config_dictionary['browserName']
    browser_version = config_dictionary['version']
    resolution = config_dictionary['screenResolution']
    filename = f'{platform}_{browser_name}_{browser_version}_{resolution}.log'
    return filename.replace(' ', '_')


def import_json(browser_configs):
    '''
    Import JSON, return Python dictionary.
    '''
    with open(browser_configs, 'r') as jfile:
        data = json.loads(jfile.read())
    return data


def main():
    ''' Launch a test instance per browser config. '''

    configs = import_json(BROWSER_CONFIGS)
    num_browsers = len(configs['browsers'])
    processes = []
    for counter in range(num_browsers):
        log_file = build_log_filename(configs['browsers'][counter])
        cmd = (f'CONFIG_FILE={BROWSER_CONFIGS} '
               f'TASK_ID={counter} '
               f'RUN_MODE={RUN_MODE} '
               f'behave --junit --junit-directory . {FEATURE_FILE} '
               f'2>&1 | tee {log_file}')
        print(cmd)
        processes.append(subprocess.Popen(cmd, shell=True))

    for counter in range(num_browsers):
        processes[counter].wait()


if __name__ == "__main__":
    try:
        BROWSER_CONFIGS = sys.argv[1]
        FEATURE_FILE = sys.argv[2]
        RUN_MODE = sys.argv[3]
    except IndexError:
        print('Browser configs (.json), feature file, and run mode '
              'are required. Use absolute paths when/if necessary.'
              'Run mode can be "local" or "jenkins".\n\n'
              'Example: python3 behave_runner.py browsers.json '
              'features/single.feature local')
        sys.exit(1)
    main()
