#!/usr/bin/env python3

'''
Dragos Reset Password Page Object Module
'''


RESET_PASSWORD_LINK = '//*[@id="external-form"]/div[6]/a'
RESET_PASSWORD_USERNAME_FIELD_ID = 'username'
OLD_PASSWORD_FIELD_ID = 'oldPassword'
NEW_PASSWORD_FIELD_ID = 'newPassword'
CONF_NEW_PASSWORD = 'confirmPassword'
RESET_PASSWORD_BUTTON_XPATH = '//*[@id="external-form"]/button'
ERROR_MESSAGE_CLASS = 'form-error'


def assertion_failed(context):
    ''' Sauce reporting - Fail '''
    context.browser.execute_script('sauce:job-result=failed')
    assert False


def assertion_passed(context):
    ''' Sauce reporting - Pass '''
    context.browser.execute_script('sauce:job-result=passed')


def get_reset_password_link(context):
    ''' Get reset password link '''
    return context.browser.find_element_by_xpath(RESET_PASSWORD_LINK)


def get_old_password(context):
    ''' Get old password '''
    return context.browser.find_element_by_id(OLD_PASSWORD_FIELD_ID)


def get_password_reset_username_field(context):
    ''' Get password reset username field '''
    return context.browser.find_element_by_id(RESET_PASSWORD_USERNAME_FIELD_ID)


def get_new_password(context):
    ''' Get new password '''
    return context.browser.find_element_by_id(NEW_PASSWORD_FIELD_ID)


def get_conf_new_password(context):
    ''' Get conf new password '''
    return context.browser.find_element_by_id(CONF_NEW_PASSWORD)


def get_reset_password_button(context):
    ''' Get reset password button '''
    return context.browser.find_element_by_xpath(RESET_PASSWORD_BUTTON_XPATH)


def get_password_reset_error(context):
    ''' Get password reset error '''
    return context.browser.find_element_by_class_name(ERROR_MESSAGE_CLASS)


def send_new_reset_password(password_field, value):
    ''' Send new reset password '''
    password_field.send_keys(value)
