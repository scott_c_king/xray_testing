#!/usr/bin/env python3

'''
Dragos Asset Page Object Module
'''

from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC


ASSET_COLUMN_HEADER_XPATH = {
    'asset': '//*[@id="asset-explorer-table"]/div/div[1]/div[1]/div[1]/div/div[2]',
    'type': '//*[@id="asset-explorer-table"]/div/div[1]/div[1]/div[1]/div/div[3]/div[1]',
    'vendor': '//*[@id="asset-explorer-table"]/div/div[1]/div[1]/div[1]/div/div[4]/div[1]',
    'mac': '//*[@id="asset-explorer-table"]/div/div[1]/div[1]/div[1]/div/div[5]/div[1]',
    'ip': '//*[@id="asset-explorer-table"]/div/div[1]/div[1]/div[1]/div/div[6]/div[1]',
    'domain': '//*[@id="asset-explorer-table"]/div/div[1]/div[1]/div[1]/div/div[7]/div[1]',
    'hostname': ('/html/body/div[1]/div/div/div/div[4]/div/div/div/div/div/div[1]/div/div/div[4]/'
                 'div/div[1]/div[1]/div[1]/div/div[8]/div[1]'),
    'zone': '//*[@id="asset-explorer-table"]/div/div[1]/div[1]/div[1]/div/div[9]/div[1]',
    'networks': '//*[@id="asset-explorer-table"]/div/div[1]/div[1]/div[1]/div/div[10]/div[1]',
    'created_at': '//*[@id="asset-explorer-table"]/div/div[1]/div[1]/div[1]/div/div[11]/div[1]'
}

ZONE_COLUMN_NAME_XPATH = '//*[@id="zone-management"]/div[2]/div[1]/div[1]/div[2]/div[1]'
ZONE_ASSET_CRITERIA_HEADER = '//*[@id="zone-management"]/div[2]/div[1]/div[1]/div[3]/div[1]'
ZONE_ASSET_ID_LOCATORS = {'zone_export_popup_id': 'open-export-modal__button'}
EXPORT_BUTTON_XPATH = ('//*[@id="root"]/div/div/div/div/div[4]/div/div/div/div/div/div[1]/div/div/div[1]/'
                       'div/div[1]/div/div[1]/div/button')
ASSET_COLUMN_EXPORT_BUTTON_XPATH = ('//*[@id="root"]/div/div/div/div/div[4]/div/div/div/div/div/div[2]/div/'
                                    'div/div[1]/div/div[3]/div/button')
COLUMN_PICKER_BUTTON_XPATH = '//*[@id="dropdown-menu__dropdown-content"]/li[2]/a'
NETWORK_LIST_XPATH = '//*[@id="select-all"]'
SELECT_NETWORK_SAVE_AND_RELOAD_BUTTON_XPATH = ('//*[@id="filters-dropdown-control__container"]/div/'
                                               'div[2]/div[2]/div[2]/button[2]')
SELECT_NETWORK_CANCEL_BUTTON_CANCEL = ('//*[@id="filters-dropdown-control__container"]/div/div[2]/'
                                       'div[2]/div[2]/button[2]')
MAC_HEADER_XPATH = '//*[@id="asset-explorer-table"]/div/div[1]/div[1]/div[1]/div/div[5]/div[1]'
ZONES_TAB_ID = 'tabs-zones-1'
ZONE_COLUMN_HEADER_XPATH = '//*[@id="zone-management"]/div[2]/div[1]/div[1]/div[2]/div[1]'
NEW_ZONE_BUTTON_ID = 'new-zone-button'
NEW_ZONE_POPUP_ID = 'create-zone-modal__content'
ZONE_REFRESH_BUTTON_ID = 'refresh-zone-button'
COLUMN_PICKER_POPUP_ID = 'column-picker-modal__content'
ZONE_EXPORT_BUTTON_ID = 'open-export-modal__button'
ASSET_REFRESH_BUTTON_XPATH = ('/html/body/div[1]/div/div/div/div[4]/div/div/div/div/div/div[1]/div/div/'
                              'div[2]/div[1]/div[1]/div[2]/button/span')


def assertion_failed(context):
    ''' Sauce reporting - Fail '''
    context.browser.execute_script('sauce:job-result=failed')
    assert False


def assertion_passed(context):
    ''' Sauce reporting - Pass '''
    context.browser.execute_script('sauce:job-result=passed')


def verify_asset_header(context):
    ''' Verify asset header '''
    asset_header_text = context.browser.find_element(By.XPATH, ASSET_COLUMN_HEADER_XPATH['asset']).text
    try:
        assert asset_header_text == 'Asset'
        assertion_passed(context)
    except AssertionError:
        assertion_failed(context)


def verify_type_header(context):
    ''' Verify type header '''
    type_header_text = context.browser.find_element(By.XPATH, ASSET_COLUMN_HEADER_XPATH['type']).text
    try:
        assert type_header_text == 'Type'
        assertion_passed(context)
    except AssertionError:
        assertion_failed(context)


def verify_vendor_header(context):
    ''' Verify vendor header '''
    vendor_header_text = context.browser.find_element(By.XPATH, ASSET_COLUMN_HEADER_XPATH['vendor']).text
    try:
        assert vendor_header_text == 'Vendor'
        assertion_passed(context)
    except AssertionError:
        assertion_failed(context)


def verify_mac_header(context):
    ''' Verify MAC header '''
    mac_header_text = context.browser.find_element(By.XPATH, ASSET_COLUMN_HEADER_XPATH['mac']).text
    try:
        assert mac_header_text == 'MAC'
        assertion_passed(context)
    except AssertionError:
        assertion_failed(context)


def verify_ip_header(context):
    ''' Verify IP header '''
    ip_header_text = context.browser.find_element(By.XPATH, ASSET_COLUMN_HEADER_XPATH['ip']).text
    try:
        assert ip_header_text == 'IP'
        assertion_passed(context)
    except AssertionError:
        assertion_failed(context)


def verify_domain_header(context):
    ''' Verify domain header '''
    domain_header_text = context.browser.find_element(By.XPATH, ASSET_COLUMN_HEADER_XPATH['domain']).text
    try:
        assert domain_header_text == 'Domain'
        assertion_passed(context)
    except AssertionError:
        assertion_failed(context)


def verify_hostname_header(context):
    ''' Verify hostname header '''
    hostname_header_text = context.browser.find_element(By.XPATH, ASSET_COLUMN_HEADER_XPATH['hostname']).text
    try:
        assert hostname_header_text == 'Hostname'
        assertion_passed(context)
    except AssertionError:
        assertion_failed(context)


def verify_zone_header(context):
    ''' Verify zone header '''
    networks_header_text = context.browser.find_element(By.XPATH, ASSET_COLUMN_HEADER_XPATH['zone']).text
    try:
        assert networks_header_text == 'Zone'
        assertion_passed(context)
    except AssertionError:
        assertion_failed(context)


def verify_networks_header(context):
    ''' Verify networks header '''
    external_header_text = context.browser.find_element(By.XPATH, ASSET_COLUMN_HEADER_XPATH['networks']).text
    try:
        assert external_header_text == 'Networks'
        assertion_passed(context)
    except AssertionError:
        assertion_failed(context)


def verify_created_at_header(context):
    ''' Verify created at header '''
    threat_header_text = context.browser.find_element(By.XPATH, ASSET_COLUMN_HEADER_XPATH['created_at']).text
    try:
        assert threat_header_text == 'Created At'
        assertion_passed(context)
    except AssertionError:
        assertion_failed(context)


def verify_zone_name_column_header(context):
    ''' Verify zone name column header '''
    name_zone_header = context.browser.find_element(By.XPATH, ZONE_COLUMN_NAME_XPATH)
    try:
        assert name_zone_header.is_displayed()
        assertion_passed(context)
    except AssertionError:
        assertion_failed(context)


def verify_asset_criteria_column_header(context):
    ''' Verify asset criteria column header '''
    asset_criteria_zone_header = context.browser.find_element(By.XPATH, ZONE_ASSET_CRITERIA_HEADER)
    try:
        assert asset_criteria_zone_header.is_displayed()
        assertion_passed(context)
    except AssertionError:
        assertion_failed(context)


def get_export_popup(context):
    ''' Get zone export pop-up '''
    return context.browser.find_element(By.ID, ZONE_ASSET_ID_LOCATORS['zone_export_popup_id'])


def click_export_column_button(context):
    ''' Export column button '''
    export_column_button = context.browser.find_element(By.XPATH, ASSET_COLUMN_EXPORT_BUTTON_XPATH)
    export_column_button.click()


def click_asset_column_button(context):
    ''' Asset column button '''
    try:
        WebDriverWait(context.browser, 5).until(
            EC.presence_of_element_located((By.XPATH, COLUMN_PICKER_BUTTON_XPATH))
        )
    finally:
        asset_column_picker_button = context.browser.find_element(By.XPATH, COLUMN_PICKER_BUTTON_XPATH)
        asset_column_picker_button.click()


def get_asset_select_network_save_button(context):
    ''' Network save button '''
    return context.browser.find_element(By.XPATH, SELECT_NETWORK_SAVE_AND_RELOAD_BUTTON_XPATH)


def get_asset_zone_tab(context):
    ''' Asset zone tab '''
    return context.browser.find_element(By.ID, ZONES_TAB_ID)


def get_asset_network_select_all_button(context):
    ''' Network select all button '''
    return context.browser.find_element(By.XPATH, NETWORK_LIST_XPATH)


def get_asset_new_zone_button(context):
    ''' New zone button '''
    return context.browser.find_element(By.ID, NEW_ZONE_BUTTON_ID)


def get_asset_create_new_zone_popup(context):
    ''' New zone popup '''
    return context.browser.find_element(By.ID, NEW_ZONE_POPUP_ID)


def get_zones_refresh_button(context):
    ''' Zones refresh button '''
    return context.browser.find_element(By.ID, ZONE_REFRESH_BUTTON_ID)


def get_asset_refresh_button(context):
    ''' Asset refresh button '''
    return context.browser.find_element(By.XPATH, ASSET_REFRESH_BUTTON_XPATH)


def get_zone_export_button(context):
    ''' Zone export button '''
    return context.browser.find_element(By.ID, ZONE_EXPORT_BUTTON_ID)
