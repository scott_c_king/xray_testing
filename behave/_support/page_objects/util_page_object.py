#!/usr/bin/env python3

'''
Dragos Util Page Object Module
'''

import time
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from selenium.common.exceptions import NoSuchElementException


# location variables
USER_BUTTON_XPATH = '//*[@id="right-menubar__menu-controls__collapsed-controls__help-menu"]'
LOGOUT_BUTTON_XPATH = '/html/body/div[1]/div/div/div/div[4]/div/nav/div[2]/div/div[3]/ul/li[2]'
RIGHT_PANEL_COLLAPSED_STYLE_ATTRIBUTES = "width: 90px; padding-left: 90px;"
APP_SIDE_CLASS_NAME = "Detections-panel"
USERNAME = 'username'
PASSWORD = 'password'


def assertion_failed(context):
    ''' Sauce reporting - Fail '''
    context.browser.execute_script('sauce:job-result=failed')
    assert False


def assertion_passed(context):
    ''' Sauce reporting - Pass '''
    context.browser.execute_script('sauce:job-result=passed')


def logout_of_the_platform(context):
    ''' Logout of platform '''
    time.sleep(10)
    try:
        WebDriverWait(context.browser, 40).until(EC.visibility_of_element_located((By.XPATH, USER_BUTTON_XPATH)))
        userbutton = context.browser.find_element(By.XPATH, USER_BUTTON_XPATH)
        userbutton.click()
        assertion_passed(context)
    except (TimeoutException, NoSuchElementException, AssertionError):
        assertion_failed(context)

    try:
        WebDriverWait(context.browser, 40).until(EC.visibility_of_element_located((By.XPATH, LOGOUT_BUTTON_XPATH)))
        time.sleep(6)
        logout_button = context.browser.find_element(By.XPATH, LOGOUT_BUTTON_XPATH)
        logout_button.click()
        time.sleep(1)
        assertion_passed(context)
    except (TimeoutException, NoSuchElementException, AssertionError):
        assertion_failed(context)


# works for left and right
def find_which_side_of_app_is_open(context, app_side):
    ''' Find which side of app is open '''
    # set class name variable
    # find item unique to asset explorer side
    # make sure that the item is visible
    try:
        WebDriverWait(context.browser, 10).until(EC.visibility_of_element_located((By.CLASS_NAME,
                                                                                   APP_SIDE_CLASS_NAME)))
        if app_side == "right":
            panel = context.browser.find_element_by_class_name("left-panel")
            # .size returns a dict
            panel_dict = panel.size
            # access the width item in the panel_dict
            panel_width = panel_dict["width"]
            # assert the opposite panel should be 0
            assert panel_width == 0
        else:
            panel = context.browser.find_element_by_class_name("right-panel")
            # using get_attribute to check the style of the right panel
            # using the width of the panel doesn't work if switching due to weird wait stuff
            # can''t wait for elements to be stale/appear
            # why the crap are implicit waits not working?
            panel_style = panel.get_attribute("style")
            # assert the opposite panel should be the style settings for the right-panel
            # this is lazy
            # find a better way
            print(APP_SIDE_CLASS_NAME)
            print(panel_style)
            assert panel_style == RIGHT_PANEL_COLLAPSED_STYLE_ATTRIBUTES
            assertion_passed(context)
    except (TimeoutException, NoSuchElementException, AssertionError):
        assertion_failed(context)


def verify_logged_out(context):
    ''' Verify logged out '''
    try:
        WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.NAME, USERNAME)))
        # password_field = context.browser.find_element(By.NAME, 'password')
        assert context.browser.find_element(By.NAME, PASSWORD)
        assertion_passed(context)
    except (TimeoutException, NoSuchElementException, AssertionError):
        assertion_failed(context)
