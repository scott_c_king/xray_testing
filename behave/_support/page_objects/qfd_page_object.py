#!/usr/bin/env python3

'''
Dragos QFD Page Object Module
'''

from selenium.webdriver.common.by import By


QFD_EXPORT_BUTTON_ID = 'open-export-modal__button'
QFD_COLUMN_PICKER_BUTTON_ID = 'column-picker__button'
QFD_EXPORT_TOGGLE_XPATH = '//*[@id="export-modal__content"]/div[1]/div/label/input'
QFD_EXPORT_TOGGLE_XPATH_TO_CLICK = '//*[@id="export-modal__content"]/div[1]/div/label'
QFD_EXPORT_COLUMN_NAME_CHECKBOX_ID = 'cb-Name132'


def assertion_failed(context):
    ''' Sauce reporting - Fail '''
    context.browser.execute_script('sauce:job-result=failed')
    assert False


def assertion_passed(context):
    ''' Sauce reporting - Pass '''
    context.browser.execute_script('sauce:job-result=passed')


def get_qfd_export_button(context):
    ''' Get QFD export button '''
    return context.browser.find_element(By.ID, QFD_EXPORT_BUTTON_ID)


def get_qfd_column_picker_button(context):
    ''' Get QFD column picker button '''
    return context.browser.find_element(By.ID, QFD_COLUMN_PICKER_BUTTON_ID)


# the xpath to the toggle ewlement that has a is_selected property
def get_qfd_export_toggle(context):
    ''' Get QFD export toggle '''
    return context.browser.find_element_by_xpath(QFD_EXPORT_TOGGLE_XPATH)


# xpath to where the toggle needs to be clicked. the is_selected element can't
# be clicked for this
def get_qfd_export_toggle_to_click(context):
    ''' Get QFD export toggle to click '''
    return context.browser.find_element_by_xpath(QFD_EXPORT_TOGGLE_XPATH_TO_CLICK)


def get_qfd_export_column_name_checkbox(context):
    ''' Get QFD column name checkbox '''
    return context.browser.find_element(By.ID, QFD_EXPORT_COLUMN_NAME_CHECKBOX_ID)
