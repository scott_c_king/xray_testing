#!/usr/bin/env python3

'''
Dragos Main Menu Module
'''

from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import TimeoutException
from selenium.common.exceptions import NoSuchElementException


STAR_BUTTON_XPATH = '//*[@id="right-menubar__menu-controls__collapsed-controls__link-homepage"]'


def assertion_failed(context):
    ''' Sauce reporting - Fail '''
    context.browser.execute_script('sauce:job-result=failed')
    assert False


def assertion_passed(context):
    ''' Sauce reporting - Pass '''
    context.browser.execute_script('sauce:job-result=passed')


def find_star_button(context):
    ''' Find star button '''
    return context.browser.find_element(By.XPATH, STAR_BUTTON_XPATH)


def verify_that_the_star_is_unfilled(context):
    ''' verify start is unfilled '''
    try:
        WebDriverWait(context.browser, 10).until(
            EC.text_to_be_present_in_element((By.XPATH, STAR_BUTTON_XPATH), 'star_border')
        )
        star_button = context.browser.find_element(By.XPATH, STAR_BUTTON_XPATH)
        star_title = star_button.text
        print(star_title)
        assert star_title == 'star_border'
        assertion_passed(context)
    except (TimeoutException, NoSuchElementException, AssertionError):
        assertion_failed(context)
