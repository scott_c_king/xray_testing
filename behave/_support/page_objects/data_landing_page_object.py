#!/usr/bin/env python3

'''
Dragos Data Landing Page Object Module
'''

from selenium.webdriver.common.by import By


QFD_LAUNCH_BUTTON_XPATH = '//*[@id="root"]/div/div/div/div/div[4]/div/div/div/div/div[1]/div/div[2]/div[2]/button'


def assertion_failed(context):
    ''' Sauce reporting - Fail '''
    context.browser.execute_script('sauce:job-result=failed')
    assert False


def assertion_passed(context):
    ''' Sauce reporting - Pass '''
    context.browser.execute_script('sauce:job-result=passed')


def get_qfd_launch_button(context):
    ''' Get QFD launch button '''
    return context.browser.find_element(By.XPATH, QFD_LAUNCH_BUTTON_XPATH)
