#!/usr/bin/env python3

'''
Dragos Reports Page Object Module
'''

from selenium.webdriver.common.by import By


REPORTS_COLUMN_IDS = {'name_header': 'report-name__table-header',
                      'date_created': 'report-created__table-header',
                      'report_type': 'report-type__table-header'}

BUTTON_IDS = {'column_filter_button': 'column-picker__button',
              'export_button': 'open-export-modal__button',
              'create_new': 'create-report__button'}

REPORTS_POPUP_IDS = {'export_popup': 'export-modal__content',
                     'create_report': 'modal__content',
                     'column_picker': 'column-picker-modal__content'}


def assertion_failed(context):
    ''' Sauce reporting - Fail '''
    context.browser.execute_script('sauce:job-result=failed')
    assert False


def assertion_passed(context):
    ''' Sauce reporting - Pass '''
    context.browser.execute_script('sauce:job-result=passed')


def verify_reports_name_column_header(context):
    ''' Verify reports name column header '''
    name_header = context.browser.find_element(By.ID, REPORTS_COLUMN_IDS['name_header']).text
    try:
        assert name_header == 'Name'
        assertion_passed(context)
    except AssertionError:
        assertion_failed(context)


def verify_reports_date_created_column_header(context):
    ''' Verify reports date created column header '''
    date_created = context.browser.find_element(By.ID, REPORTS_COLUMN_IDS['date_created']).text
    try:
        assert date_created == 'Date Created'
        assertion_passed(context)
    except AssertionError:
        assertion_failed(context)


def verify_reports_report_type_column_header(context):
    ''' Verify reports report type column header '''
    report_type = context.browser.find_element(By.ID, REPORTS_COLUMN_IDS['report_type']).text
    try:
        assert report_type == 'Report Type'
        assertion_passed(context)
    except AssertionError:
        assertion_failed(context)


def verify_reports_report_column_filter(context):
    ''' Verify reports report column filter '''
    column_filter_button = get_column_filter_button(context)
    try:
        assert column_filter_button.is_displayed()
        assertion_passed(context)
    except AssertionError:
        assertion_failed(context)


def verify_reports_export_button(context):
    ''' Verify reports export button '''
    column_filter_button = get_export_report_button(context)
    try:
        assert column_filter_button.is_displayed()
        assertion_passed(context)
    except AssertionError:
        assertion_failed(context)


def verify_reports_create_new_button(context):
    ''' Verify reports create new button '''
    create_new_button = get_create_new_report_button(context)
    try:
        assert create_new_button.is_displayed()
        assertion_passed(context)
    except AssertionError:
        assertion_failed(context)


def get_create_new_report_button(context):
    ''' Get create new report button '''
    return context.browser.find_element(By.ID, BUTTON_IDS['create_new'])


def get_export_report_button(context):
    ''' Get export report button '''
    return context.browser.find_element(By.ID, BUTTON_IDS['export_button'])


def get_export_popup(context):
    ''' Get export popup '''
    return context.browser.find_element(By.ID, REPORTS_POPUP_IDS['export_popup'])


def get_create_new_report_popup(context):
    ''' Get create new report popup '''
    return context.browser.find_element(By.ID, REPORTS_POPUP_IDS['create_report'])


def get_column_filter_button(context):
    ''' Get column filter button '''
    return context.browser.find_element(By.ID, BUTTON_IDS['column_filter_button'])


def get_column_filter_popup(context):
    ''' Get column filter popup '''
    return context.browser.find_element(By.ID, REPORTS_POPUP_IDS['column_picker'])
