#!/usr/bin/env python3

'''
Dragos Detection Dashboard Page Object Module
'''

import time
import random
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from selenium.common.exceptions import NoSuchElementException
from faker import Faker


FAKE = Faker()
MODELING_TITLE_XPATH = '//*[@id="root"]/div/div/div/div/div[2]/div/div/div[2]/div[1]/div/div/div[1]/div[1]/div[1]'
THREAT_TITLE_XPATH = '//*[@id="root"]/div/div/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[1]/div[1]/div[1]'
CONFIG_TITLE_XPATH = '//*[@id="root"]/div/div/div/div/div[2]/div/div/div[2]/div[3]/div/div/div[1]/div[1]/div[1]'
INDICATOR_TITLE_XPATH = '//*[@id="root"]/div/div/div/div/div[2]/div/div/div[2]/div[4]/div/div/div[1]/div[1]/div[1]'
MODULE_TITLES_XPATHS = (MODELING_TITLE_XPATH, THREAT_TITLE_XPATH, CONFIG_TITLE_XPATH, INDICATOR_TITLE_XPATH)
PRIORITY_FIELD_XPATH = ('/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div[2]/div/div[2]/div[2]/'
                        'div[1]/div[2]/div[1]/div/div/input')
PRIORITY_2_VALUE_XPATH = ('/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div[2]/div/div[2]/div[2]/'
                          'div[1]/div[2]/div[1]/div/ul/li[3]')
VISIBILITY_PUBLIC_XPATH = ('/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div[2]/div/div[2]/div[2]/'
                           'div[1]/div[2]/div[2]/div/ul/li[1]')
VISIBILITY_FIELD_XPATH = ('/html/body/div/div/div/div/div/div[2]/div/div/div[2]/div[2]/div/div[2]/div[2]/'
                          'div[1]/div[2]/div[2]/div/div/input')
RIGHT_FULL_SLIDE_BUTTON = '//*[@id="primary-nav"]/div[2]/div/div[3]/div[2]/i'
LEFT_FULL_SLIDE_BUTTON = '//*[@id="primary-nav"]/div[2]/div/div[3]/div[1]/i'
POPUP_CLASS_NAME = 'modal-content'
FIRST_THREAT_NOTIFICATION = ('//*[@id="root"]/div/div/div/div/div[2]/div/div/div[2]/div[2]/div/div/'
                             'div[2]/div/div[1]/ul/li[1]/div')
STYLING_IF_BAR_GRAPH_IS_SHOWN = "width: 200%; margin-left: -100%;"
CREATE_A_CASE_BUTTON_XPATH = ('//*[@id="root"]/div/div/div/div/div[2]/div/div/div[2]/div[2]/div/div[2]/'
                              'div[2]/div[2]/div/div[2]/button[1]')
NOTIFICATION_POPUP_LIST_OF_FIELDS = ('//label[text()="Description"]', '//label[text()="Occurred At"]',
                                     '//label[text()="Source"]', '//label[text()="Type"]',
                                     '//label[text()="Detected By"]', '//label[text()="Playbooks"]',
                                     '//label[text()="Assets"]', '//label[text()="Associated Notifications"]')
MODELING_INFORMATION_ICON_ID = 'information-popover'
THREAT_BEHAVIOR_INFORMATION_ICON_XPATH = '//*[@id="information-popover"]/i'
MODELING_INFORMATION_TEXT_XPATH = '//*[@id="information-popover"]/div'
MODELING_NOTIFICATION_MENU_BUTTON_XPATH = ('//*[@id="detection-dashboard"]/div[2]/div[1]/div/div/div[1]/'
                                           'div[1]/div[2]/div[2]/div')
MODELING_NOTIFICATION_MENU_XPATH = ('//*[@id="detection-dashboard"]/div[2]/div[1]/div/div/div[1]/div[1]/'
                                    'div[2]/div[2]/ul')
TEST_CASE_NAME = 'test case #' + str(random.randint(1, 5000))
TEST_CASE_HYPOTHESIS = FAKE.sentence() + FAKE.bs()

# Detections page variables
DETECTIONS_DATE_PICKER_FROM_FIELD_XPATH = '//*[@id="time-range-controls"]/div/div[1]/input'
DETECTIONS_DATE_PICKER_BACK_MONTH_ARROW_XPATH = '/html/body/div[4]/div[1]/span[1]'

# used to select the first day of January in calendar
DETECTIONS_DATE_PICKER_JAN1_XPATH = '/html/body/div[4]/div[2]/div/div[2]/div/span[4]'
CREATE_CASE_DATA = {'name': TEST_CASE_NAME, 'hypothesis': TEST_CASE_HYPOTHESIS}


def assertion_failed(context):
    ''' Sauce reporting - Fail '''
    context.browser.execute_script('sauce:job-result=failed')
    assert False


def assertion_passed(context):
    ''' Sauce reporting - Pass '''
    context.browser.execute_script('sauce:job-result=passed')


def is_module_present(context, modeling_module_xpath):
    ''' Is module present '''
    try:
        WebDriverWait(context.browser, 5).until(
            EC.presence_of_element_located((By.XPATH, modeling_module_xpath))
        )
        module = context.browser.find_element(By.XPATH, modeling_module_xpath)
        assert module.is_displayed()
        assertion_passed(context)
    except (TimeoutException, NoSuchElementException, AssertionError):
        assertion_failed(context)


# can this be prettier?
# make a dict and loop through it?
def assert_module_names(context):
    ''' Assert module names '''
    try:
        modeling_title = context.browser.find_element(By.XPATH, MODULE_TITLES_XPATHS[0]).text
        threat_title = context.browser.find_element(By.XPATH, MODULE_TITLES_XPATHS[1]).text
        config_title = context.browser.find_element(By.XPATH, MODULE_TITLES_XPATHS[2]).text
        indicator_title = context.browser.find_element(By.XPATH, MODULE_TITLES_XPATHS[3]).text
        assert modeling_title == 'MODELING'
        assert threat_title == 'THREAT BEHAVIOR'
        assert config_title == 'CONFIGURATION'
        assert indicator_title == 'INDICATOR'
        assertion_passed(context)
    except (NoSuchElementException, AssertionError):
        assertion_failed(context)


def click_on_full_slide_arrow(context, arrow_side):
    ''' Click on full slide arrow '''
    try:
        WebDriverWait(context.browser, 10).until(
            EC.presence_of_element_located((By.XPATH, RIGHT_FULL_SLIDE_BUTTON))
        )
        if arrow_side == 'right':
            right_slide_button = context.browser.find_element(By.XPATH, RIGHT_FULL_SLIDE_BUTTON)
            right_slide_button.click()
        else:
            left_slide_button = context.browser.find_element(By.XPATH, LEFT_FULL_SLIDE_BUTTON)
            left_slide_button.click()
            assertion_passed(context)
    except (TimeoutException, NoSuchElementException, AssertionError):
        assertion_failed(context)


def did_the_graph_change_to_the_bar_graph(context, graph):
    ''' Graph bar changes '''
    try:
        WebDriverWait(context.browser, 10).until(
            EC.presence_of_element_located((By.XPATH, graph))
        )
        graph_element = context.browser.find_element(By.XPATH, graph)
        graph_style = graph_element.get_attribute("style")
        assert graph_style == STYLING_IF_BAR_GRAPH_IS_SHOWN
        assertion_passed(context)
    except (TimeoutException, NoSuchElementException, AssertionError):
        assertion_failed(context)


def assert_how_many_items_are_in_notification_list_page(context, notification_list_css):
    ''' Check items in notification list '''
    try:
        WebDriverWait(context.browser, 10).until(
            EC.presence_of_element_located((By.CSS_SELECTOR, notification_list_css))
        )
        notification_list = context.browser.find_elements_by_css_selector(notification_list_css)
        assert len(notification_list) == 20
        assertion_passed(context)
    except (TimeoutException, NoSuchElementException, AssertionError):
        assertion_failed(context)


def click_the_first_notification(context):
    ''' Check first notification '''
    try:
        WebDriverWait(context.browser, 10).until(
            EC.presence_of_element_located((By.XPATH, FIRST_THREAT_NOTIFICATION))
        )
        notification = context.browser.find_element(By.XPATH, FIRST_THREAT_NOTIFICATION)
        notification.click()
        assertion_passed(context)
    except (TimeoutException, NoSuchElementException, AssertionError):
        assertion_failed(context)


def assert_popup_appeared(context):
    ''' Popup appeared '''
    try:
        WebDriverWait(context.browser, 10).until(
            EC.presence_of_element_located((By.CLASS_NAME, POPUP_CLASS_NAME))
        )
        popup = context.browser.find_element(By.CLASS_NAME, POPUP_CLASS_NAME)
        assert popup.is_displayed()
        assertion_passed(context)
    except (TimeoutException, NoSuchElementException, AssertionError):
        assertion_failed(context)


def verify_notification_popup_fields(context):
    ''' Verify notification popup fields '''
    try:
        WebDriverWait(context.browser, 10).until(
            EC.presence_of_element_located((By.CLASS_NAME, POPUP_CLASS_NAME))
        )
        for _ in NOTIFICATION_POPUP_LIST_OF_FIELDS:
            description_label = context.browser.find_element(By.XPATH, _)
            assert description_label.is_displayed()
        assertion_passed(context)
    except (TimeoutException, NoSuchElementException, AssertionError):
        assertion_failed(context)


def find_create_a_case_button(context):
    ''' Get create a case button xpath '''
    return context.browser.find_element(By.XPATH, CREATE_A_CASE_BUTTON_XPATH)


def get_test_case_name_field(context):
    ''' Get test case name field '''
    return context.browser.find_element(By.NAME, 'name')


def get_test_case_hypothesis_field(context):
    ''' Get test case hypothesis field '''
    return context.browser.find_element(By.NAME, 'hypothesis')


def send_valid_info(context):
    ''' Send valid info '''
    try:
        name = get_test_case_name_field(context)
        name.send_keys(CREATE_CASE_DATA['name'])
        hypothesis = get_test_case_hypothesis_field(context)
        hypothesis.send_keys(CREATE_CASE_DATA['hypothesis'])
        time.sleep(2)
        select_value_from_dropdown(context, PRIORITY_FIELD_XPATH, PRIORITY_2_VALUE_XPATH)
        select_value_from_dropdown(context, VISIBILITY_FIELD_XPATH, VISIBILITY_PUBLIC_XPATH)
        time.sleep(2)
        assertion_passed(context)
    except NoSuchElementException:
        assertion_failed(context)


def assert_create_test_case_appears(context):
    ''' Assert create test case '''
    try:
        WebDriverWait(context.browser, 5).until(
            EC.presence_of_element_located((By.NAME, 'hypothesis'))
        )
        hypothesis_field = get_test_case_hypothesis_field(context)
        assert hypothesis_field.is_displayed()
        assertion_passed(context)
    except (TimeoutException, NoSuchElementException, AssertionError):
        assertion_failed(context)


def select_value_from_dropdown(context, field_xpath, value_xpath):
    ''' Select value from dropdown '''
    priority = context.browser.find_element(By.XPATH, field_xpath)
    priority.click()
    try:
        WebDriverWait(context.browser, 5).until(
            EC.presence_of_element_located((By.XPATH, value_xpath))
        )
        value = context.browser.find_element(By.XPATH, value_xpath)
        value.click()
    except (TimeoutException, NoSuchElementException, AssertionError):
        assertion_failed(context)


def get_information_icon(context):
    ''' Get information icon '''
    return context.browser.find_elements_by_id(MODELING_INFORMATION_ICON_ID)


def get_information_text(context):
    ''' Get information text '''
    return context.browser.find_elements_by_xpath(MODELING_INFORMATION_TEXT_XPATH)


def get_modeling_notification_button(context):
    ''' Get modeling notification button xpath '''
    return context.browser.find_element(By.XPATH, MODELING_NOTIFICATION_MENU_BUTTON_XPATH)


def get_modeling_notification_menu(context):
    ''' Get modeling notification menu xpath '''
    return context.browser.find_element(By.XPATH, MODELING_NOTIFICATION_MENU_XPATH)


def get_detections_dashboard_from_field(context):
    ''' Get detections dashboard from field '''
    return context.browser.find_element(By.XPATH, DETECTIONS_DATE_PICKER_FROM_FIELD_XPATH)


def get_detections_dashboard_from_current_month_value(context):
    ''' Get detections dashboard from current month value '''
    return context.browser.find_element_by_class_name("cur-month").text


def get_detections_calendar_back_arrow(context):
    ''' Get detections calendar back arrow xpath '''
    return context.browser.find_element(By.XPATH, DETECTIONS_DATE_PICKER_BACK_MONTH_ARROW_XPATH)
