#!/usr/bin/env python3

'''
Dragos Case Management Hunt View Page Object Module
'''

from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from selenium.common.exceptions import NoSuchElementException
from faker import Faker


FAKE = Faker()
TEXT_TO_ENTER_TO_JOURNAL = (FAKE.bs() + " " + FAKE.paragraph(nb_sentences=24,
                                                             variable_nb_sentences=True,
                                                             ext_word_list=None) + " " + FAKE.bs())
HEADER_CLASS_NAME = 'name-header'
CASE_MANAGEMENT_LOCATORS = {
    'visibility_field_xpath': '//*[@id="root"]/div/div/div/div/div[2]/div/div/div[1]/div[2]/div[1]',
    'priority_field_xpath': '//*[@id="root"]/div/div/div/div/div[2]/div/div/div[1]/div[2]/div[2]',
    'journal_placeholder_class_name': 'editor-placeholder',
    'journal_editor_xpath': ('//*[@id="root"]/div/div/div/div/div[2]/div/div/div[2]/div[2]/div/div/'
                             'div[5]/div/div/div[1]/div/div/div[2]/textarea'),
    'journal_submit_button_xpath': ('//*[@id="root"]/div/div/div/div/div[2]/div/div/div[2]/div[2]/'
                                    'div/div/div[5]/div/div/div[2]/div[1]/i')
}


def assertion_failed(context):
    ''' Sauce reporting - Fail '''
    context.browser.execute_script('sauce:job-result=failed')
    assert False


def assertion_passed(context):
    ''' Sauce reporting - Pass '''
    context.browser.execute_script('sauce:job-result=passed')


def get_test_case_title(context):
    ''' Returns test case title '''
    return context.browser.find_element(By.CLASS_NAME, HEADER_CLASS_NAME).text


def verify_on_the_test_case_page(context):
    ''' Verify on the test case page '''
    try:
        WebDriverWait(context.browser, 5).until(
            EC.presence_of_element_located((By.CLASS_NAME, HEADER_CLASS_NAME))
        )
        header = context.browser.find_element_by_class_name(HEADER_CLASS_NAME)
        assert header.is_displayed()
        assertion_passed(context)
    except (TimeoutException, NoSuchElementException, AssertionError):
        assertion_failed(context)


def get_test_case_visibility_field(context):
    ''' Return test case visibility field '''
    return context.browser.find_element(By.XPATH, CASE_MANAGEMENT_LOCATORS['visibility_field_xpath']).text


def get_test_case_priority(context):
    ''' Return priority field '''
    return context.browser.find_element(By.XPATH, CASE_MANAGEMENT_LOCATORS['priority_field_xpath']).text


def get_journal_field_placeholder(context):
    ''' Return journal field '''
    return context.browser.find_element(By.CLASS_NAME, CASE_MANAGEMENT_LOCATORS['journal_placeholder_class_name'])


def get_journal_editor(context):
    ''' Return journal editor '''
    return context.browser.find_element(By.XPATH, CASE_MANAGEMENT_LOCATORS['journal_editor_xpath'])


def get_journal_submit_button(context):
    ''' Return journal submit button '''
    return context.browser.find_element(By.XPATH, CASE_MANAGEMENT_LOCATORS['journal_submit_button_xpath'])


def send_keys_to_journal(journal_editor):
    ''' Send keys to journal '''
    journal_editor.send_keys(TEXT_TO_ENTER_TO_JOURNAL)


def verify_journal_entry_is_present(context):
    ''' Verify joural entry is present '''
    # xpath_with_journal_entry = "//[text()='" + TEXT_TO_ENTER_TO_JOURNAL +"']"
    journal_items = context.browser.find_elements(By.CLASS_NAME, 'human-generated')
    for _ in journal_items:
        if _.text == 'gobnblygook':
            assert True

    # assert journal_items.is_displayed()
