#!/usr/bin/env python3

'''
Dragos Create and Edit User Page Object Module
'''

import time
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException


USER_PROFILE_XPATH = '//*[@id="manage-user-modal__content"]'


def assertion_failed(context):
    ''' Sauce reporting - Fail '''
    context.browser.execute_script('sauce:job-result=failed')
    assert False


def assertion_passed(context):
    ''' Sauce reporting - Pass '''
    context.browser.execute_script('sauce:job-result=passed')


def get_id_field(context):
    ''' Get ID field '''
    return context.browser.find_element(By.ID, 'id')


def get_email_field(context):
    ''' Get email field '''
    return context.browser.find_element(By.ID, 'email')


def get_firstname_field(context):
    ''' Get firstname field '''
    return context.browser.find_element(By.ID, 'firstName')


def get_lastname_field(context):
    ''' Get lastname field '''
    return context.browser.find_element(By.ID, 'lastName')


def get_password_field(context):
    ''' Get password field '''
    return context.browser.find_element(By.ID, 'password')


def get_confirm_password_field(context):
    ''' Confirm password field '''
    return context.browser.find_element(By.ID, 'passwordConfirm')


def get_user_profile_module(context):
    ''' Get user profile xpath '''
    return context.browser.find_element(By.XPATH, USER_PROFILE_XPATH)


def verify_user_profile_is_present(context, user_profile):
    ''' Verify user profile is present '''
    time.sleep(1)
    try:
        WebDriverWait(context.browser, 10).until(
            EC.presence_of_element_located((By.XPATH, USER_PROFILE_XPATH))
        )
        assert user_profile.is_displayed()
    except (TimeoutException, AssertionError):
        assertion_failed(context)
