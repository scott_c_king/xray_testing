#!/usr/bin/env python3

'''
Dragos Login Page Module
'''


import os
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import InvalidArgumentException
from selenium.common.exceptions import TimeoutException
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import WebDriverException


VALID_USERNAME = 'admin'
VALID_PASSWORD = 'Dr@gosSyst3m'

try:
    LOGIN_URL = os.environ['SSHOST']
except KeyError:
    LOGIN_URL = "https://platform-dev10.dragos.services/#/login"

# field locator classes/ids/selectors
PASSWORD_FIELD_ID = 'password'
USERNAME_FIELD_ID = 'username'
LOGIN_BUTTON_SELECTOR = '#external-form > button'
SHOW_PASSWORD_BUTTON = '//*[@id="external-form"]/div[4]/a/small'
COPYRIGHT_CHECKBOX_XPATH = '//*[@id="external-form"]/div[5]/div[1]'
LICENSE_CHECKBOX_XPATH = '//*[@id="external-form"]/div[5]/div[1]'
LOGIN_BUTTON_XPATH = '//*[@id="external-form"]/button'
USER_LICENSE_CHECKBOX_ID = 'login__accept-eula'
USER_LICENSE_CHECKBOX_XPATH = '//*[@id="external-form"]/div[5]/div[1]'


def assertion_failed(context):
    ''' Sauce reporting - Fail '''
    context.browser.execute_script('sauce:job-result=failed')
    assert False


def assertion_passed(context):
    ''' Sauce reporting - Pass '''
    context.browser.execute_script('sauce:job-result=passed')


def find_password_field(context):
    ''' Return password field element '''
    return context.browser.find_element_by_id(PASSWORD_FIELD_ID)


def find_username_field(context):
    ''' Return username field element '''
    return context.browser.find_element_by_id(USERNAME_FIELD_ID)


def find_login_button(context):
    ''' Return login button element '''
    return context.browser.find_element_by_css_selector(LOGIN_BUTTON_SELECTOR)


def send_valid_username(username_field):
    ''' Send valid username '''
    username_field.send_keys(VALID_USERNAME)


def send_valid_password(password_field):
    ''' Send valid password '''
    password_field.send_keys(VALID_PASSWORD)


def find_copyright_checkbox(context):
    ''' Return copyright checkbox element '''
    return context.browser.find_element(By.XPATH, COPYRIGHT_CHECKBOX_XPATH)


def login_with_valid_credentials(context):
    ''' Login with valid credentials '''
    try:
        username_field = find_username_field(context)
        username_field.send_keys(VALID_USERNAME)
        password_field = find_password_field(context)
        password_field.send_keys(VALID_PASSWORD)
        copyright_checkbox = find_copyright_checkbox(context)
        copyright_checkbox.click()
        login_button = find_login_button(context)
        login_button.click()
        assertion_passed(context)
    except (NoSuchElementException, AssertionError):
        assertion_failed(context)


def get_field_type(password_field):
    ''' Get password field type '''
    return password_field.get_attribute("type")


def find_show_password_button(context):
    ''' Return password button element '''
    return context.browser.find_element(By.XPATH, SHOW_PASSWORD_BUTTON)


def enter_valid_credentials(context):
    ''' Enter valid credentials '''
    try:
        WebDriverWait(context.browser, 10).until(
            EC.presence_of_element_located((By.ID, USERNAME_FIELD_ID)))
        username_field = find_username_field(context)
        send_valid_username(username_field)
        password_field = find_password_field(context)
        send_valid_password(password_field)
        assertion_passed(context)
    except (TimeoutException, NoSuchElementException):
        assertion_failed(context)


def verify_logged_into_platform(context):
    ''' Verify successful login '''
    try:
        WebDriverWait(context.browser, 10).until(
            EC.presence_of_element_located((By.CLASS_NAME, "menu-controls")))
        assert context.browser.find_element_by_class_name("menu-controls")
        assertion_passed(context)
    except (TimeoutException, NoSuchElementException):
        assertion_failed(context)


def nav_to_platform(context):
    ''' Get Sitestore URL '''
    try:
        context.browser.get(LOGIN_URL)
        assertion_passed(context)
    except (InvalidArgumentException, WebDriverException):
        assertion_failed(context)


def find_license_checkbox(context):
    ''' Return license checkbox element '''
    return context.browser.find_element_by_xpath(LICENSE_CHECKBOX_XPATH)


def get_login_button(context):
    ''' Return login button element '''
    return context.browser.find_element_by_xpath(LOGIN_BUTTON_XPATH)


def get_user_license_checkbox(context):
    ''' Return user license checkbox element '''
    return context.browser.find_element_by_xpath(USER_LICENSE_CHECKBOX_XPATH)
