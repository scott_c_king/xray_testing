#!/usr/bin/env python3

'''
Dragos Navigation Page Object Module
'''

from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from selenium.common.exceptions import NoSuchElementException


def assertion_failed(context):
    ''' Sauce reporting - Fail '''
    context.browser.execute_script('sauce:job-result=failed')
    assert False


def assertion_passed(context):
    ''' Sauce reporting - Pass '''
    context.browser.execute_script('sauce:job-result=passed')


def click_a_button(context, button):
    ''' Button operations '''
    try:
        WebDriverWait(context.browser, 10).until(EC.presence_of_element_located((By.XPATH, button)))
        link = context.browser.find_element(By.XPATH, button)
        link.click()
        assertion_passed(context)
    except (TimeoutException, NoSuchElementException, AssertionError):
        assertion_failed(context)


def verify_page_class(context, class_name):
    ''' Verify page class '''
    try:
        WebDriverWait(context.browser, 5).until(EC.presence_of_element_located((By.CLASS_NAME, class_name)))
        assert context.browser.find_element_by_class_name(class_name)
    except (TimeoutException, NoSuchElementException, AssertionError):
        assertion_failed(context)
