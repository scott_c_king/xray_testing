#!/usr/bin/env python3

'''
Dragos Content Page Object Module
'''

from selenium.webdriver.common.by import By


# detections page
GROUP_BY_BUTTON_XPATH = '//*[@id="analytics-menu-context-dropdown"]/div/div/i'
DRAGOS_TOGGLE_BUTTON_ID = 'analytics-menu-toggle-dragos-filter'
USER_ANALYTICS_BUTTON_ID = 'analytics-menu-toggle-user-filter'
PLAY_BUTTON_ID = 'analytics-menu-toggle-running-filter'
PAUSE_BUTTON_ID = 'analytics-menu-toggle-suspended-filter'
ANALYTIC_BUTTON_ID = 'add-analytic-button'
CREATE_NEW_ANALYTIC_POPUP_ID = 'new-analytic-modal'
CHAR_TAB_ID = "tabs-characterization-1"

# char tab
# use full xpath b/c first tab is still visible technically and will create errors using ids
# these are prob gonna break though
CHAR_ANALYTIC_BUTTON_XPATH = ('/html/body/div[1]/div/div/div/div[4]/div/div/div/div/div[2]/'
                              'div/div[1]/div[1]/div[2]/div[5]/button')
CHAR_GROUP_BY_BUTTON_XPATH = ('/html/body/div[1]/div/div/div/div[4]/div/div/div/div/div[2]/'
                              'div/div[1]/div[2]/div/div/div/i')
CHAR_DRAGOS_TOGGLE_BUTTON_ID = ('/html/body/div[1]/div/div/div/div[4]/div/div/div/div/div[2]/'
                                'div/div[1]/div[1]/div[2]/div[1]/button/i')
CHAR_USER_ANALYTICS_BUTTON_ID = ('/html/body/div[1]/div/div/div/div[4]/div/div/div/div/div[2]/'
                                 'div/div[1]/div[1]/div[2]/div[1]/button/i')
CHAR_PLAY_BUTTON_ID = ('/html/body/div[1]/div/div/div/div[4]/div/div/div/div/div[2]/div/'
                       'div[1]/div[1]/div[2]/div[3]/button/i')
CHAR_PAUSE_BUTTON_ID = ('/html/body/div[1]/div/div/div/div[4]/div/div/div/div/div[2]/div/'
                        'div[1]/div[1]/div[2]/div[4]/button/i')
CHAR_ANALYTIC_BUTTON_ID = ('/html/body/div[1]/div/div/div/div[4]/div/div/div/div/div[2]/div/'
                           'div[1]/div[1]/div[2]/div[5]/button/i')
CHAR_CREATE_NEW_ANALYTIC_POPUP_ID = 'new-analytic-modal'


def assertion_failed(context):
    ''' Sauce reporting - Fail '''
    context.browser.execute_script('sauce:job-result=failed')
    assert False


def assertion_passed(context):
    ''' Sauce reporting - Pass '''
    context.browser.execute_script('sauce:job-result=passed')


def get_group_by_button(context):
    ''' Get group by button '''
    return context.browser.find_element(By.XPATH, GROUP_BY_BUTTON_XPATH)


def get_char_group_by_button(context):
    ''' Get char group by button '''
    return context.browser.find_element(By.XPATH, CHAR_GROUP_BY_BUTTON_XPATH)


def get_dragos_toggle(context):
    ''' Get Dragos toggle '''
    return context.browser.find_element(By.ID, DRAGOS_TOGGLE_BUTTON_ID)


def get_char_dragos_toggle(context):
    ''' Get char Dragos toggle '''
    return context.browser.find_element(By.XPATH, CHAR_DRAGOS_TOGGLE_BUTTON_ID)


def get_user_analytics_button(context):
    ''' Get user analytics button '''
    return context.browser.find_element(By.ID, USER_ANALYTICS_BUTTON_ID)


def get_char_user_analytics_button(context):
    ''' Get char user analytics button '''
    return context.browser.find_element(By.XPATH, CHAR_USER_ANALYTICS_BUTTON_ID)


def get_play_button(context):
    ''' Get play button '''
    return context.browser.find_element(By.ID, PLAY_BUTTON_ID)


def get_char_play_button(context):
    ''' Get char play button '''
    return context.browser.find_element(By.XPATH, CHAR_PLAY_BUTTON_ID)


def get_pause_button(context):
    ''' Get pause button '''
    return context.browser.find_element(By.ID, PAUSE_BUTTON_ID)


def get_char_pause_button(context):
    ''' Get char pause button '''
    return context.browser.find_element(By.XPATH, CHAR_PAUSE_BUTTON_ID)


def get_analytic_button(context):
    ''' Get analytic button '''
    return context.browser.find_element(By.ID, ANALYTIC_BUTTON_ID)


def get_char_analytic_button(context):
    ''' Get char analytic button '''
    return context.browser.find_element(By.XPATH, CHAR_ANALYTIC_BUTTON_ID)


def get_char_analytic_xpath(context):
    ''' Get char analytic xpath '''
    return context.browser.find_element(By.XPATH, CHAR_ANALYTIC_BUTTON_XPATH)


def get_create_new_analytic_popup(context):
    ''' Get create new analytic popup '''
    return context.browser.find_element(By.ID, CREATE_NEW_ANALYTIC_POPUP_ID)


def get_char_tab(context):
    ''' Get char tab '''
    return context.browser.find_element(By.ID, CHAR_TAB_ID)
