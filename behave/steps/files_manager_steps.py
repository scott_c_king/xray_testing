#!/usr/bin/env python3

'''
Dragos Files Manager Steps Module
'''

import time
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import TimeoutException
from behave import when, then  # pylint: disable=no-name-in-module
import _support.page_objects.qfd_page_object as qfd_po


FM_BACK_BUTTON_ID = 'back-button'
FM_EXPORT_BUTTON_ID = 'open-export-modal__button'
FM_EXPORT_MODULE_ID = 'export-modal__content'
FM_COLUMN_PICKER_POPUP_ID = 'column-picker-modal__content'
FM_COLUMN_PICKER_BUTTON_ID = 'column-picker__button'


@then('the fm page has export, back, and column filter buttons')
def verify_fm_page_buttons(context):
    ''' Verify FM page buttons '''
    try:
        time.sleep(1)
        fm_back_button = context.browser.find_element(By.ID, FM_BACK_BUTTON_ID)
        fm_export_button = context.browser.find_element(By.ID, FM_EXPORT_BUTTON_ID)
        fm_column_picker_button = context.browser.find_element(By.ID, FM_COLUMN_PICKER_BUTTON_ID)
        assert fm_back_button.is_displayed()
        assert fm_export_button.is_displayed()
        assert fm_column_picker_button.is_displayed()
        qfd_po.assertion_passed(context)
    except (NoSuchElementException, AssertionError):
        qfd_po.assertion_failed(context)


@when('I click the fm export button')
def click_fm_export_button(context):
    ''' Click the FM export button '''
    try:
        time.sleep(2)
        WebDriverWait(context.browser, 60).until(EC.element_to_be_clickable((By.ID, FM_EXPORT_BUTTON_ID)))
        fm_export_button = context.browser.find_element(By.ID, FM_EXPORT_BUTTON_ID)
        fm_export_button.click()
        qfd_po.assertion_passed(context)
    except (TimeoutException, NoSuchElementException, AssertionError):
        qfd_po.assertion_failed(context)


@then('the fm export popup appears')
def fm_popup_appears(context):
    ''' The FM export popup appears '''
    try:
        time.sleep(.5)
        fm_export_popup = context.browser.find_element(By.ID, FM_EXPORT_MODULE_ID)
        assert fm_export_popup.is_displayed()
        qfd_po.assertion_passed(context)
    except (NoSuchElementException, AssertionError):
        qfd_po.assertion_failed(context)


@when('I click the fm column picker')
def click_fm_column_picker(context):
    ''' Click the FM column picker '''
    try:
        fm_column_picker_button = context.browser.find_element(By.ID, FM_COLUMN_PICKER_BUTTON_ID)
        fm_column_picker_button.click()
        qfd_po.assertion_passed(context)
    except (NoSuchElementException, AssertionError):
        qfd_po.assertion_failed(context)


@then('the fm column picker appears')
def fm_column_picker_appears(context):
    ''' The FM column picker appears '''
    try:
        time.sleep(.5)
        fm_column_picker_popup = context.browser.find_element(By.ID, FM_COLUMN_PICKER_POPUP_ID)
        assert fm_column_picker_popup.is_displayed()
        qfd_po.assertion_passed(context)
    except AssertionError:
        qfd_po.assertion_failed(context)


@when('I click the fm back button')
def click_fm_back_button(context):
    ''' Click the FM back button '''
    try:
        time.sleep(3)
        fm_back_button = context.browser.find_element(By.ID, FM_BACK_BUTTON_ID)
        fm_back_button.click()
        qfd_po.assertion_passed(context)
    except (NoSuchElementException, AssertionError):
        qfd_po.assertion_failed(context)
