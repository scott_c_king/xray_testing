#!/usr/bin/env python3

'''
Dragos Main Menu Steps Module
'''

import time
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from behave import when, then  # pylint: disable=no-name-in-module
import _support.page_objects.main_menu as mm_po
import _support.page_objects.navigation_page_object as nav_po
import _support.page_objects.util_page_object as util_po


STAR_BUTTON_XPATH = '//*[@id="right-menubar__menu-controls__collapsed-controls__link-homepage"]'
FULLSCREEN_BUTTON_XPATH = '//*[@id="right-menubar__menu-controls__collapsed-controls__link-fullscreen"]'
CONTENT_PANEL_XPATH = '//*[@id="root"]/div/div/div/div/div[4]'


@then('the star is colored in')
def star_colored_in(context):
    ''' Star is colored in '''
    try:
        star_button = mm_po.find_star_button(context)
        star_title = star_button.text
        assert star_title == 'star'
        mm_po.assertion_passed(context)
    except (NoSuchElementException, AssertionError):
        mm_po.assertion_failed(context)


@then('the star is unfilled')
def star_unfilled(context):
    ''' Star is unfilled '''
    mm_po.verify_that_the_star_is_unfilled(context)


@when('I click the star')
def click_the_star(context):
    ''' Click the star '''
    nav_po.click_a_button(context, STAR_BUTTON_XPATH)


@then('I am logged out')
def logged_out(context):
    ''' Verify logged out '''
    util_po.verify_logged_out(context)


@when('I make the window fullscreen')
def make_fullscreen(context):
    ''' Make fullscreen '''
    nav_po.click_a_button(context, FULLSCREEN_BUTTON_XPATH)


# broken figure out why later
@then('the window is fullscreen')
def window_is_fullscreen(context):
    ''' Window is fullscreen '''
    try:
        time.sleep(2)
        dimension = 0
        panel = context.browser.find_element(By.XPATH, CONTENT_PANEL_XPATH)
        new_dimension = panel.size['width']
        time.sleep(2)
        assert dimension < new_dimension
        mm_po.assertion_passed(context)
    except (NoSuchElementException, AssertionError):
        mm_po.assertion_failed(context)
