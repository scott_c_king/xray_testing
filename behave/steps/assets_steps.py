#!/usr/bin/env python3

'''
Dragos Assets Steps Module
'''

import time
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.ui import WebDriverWait
from behave import when, then  # pylint: disable=no-name-in-module
import _support.page_objects.assets_page_object as asset_po


@then('the correct Asset column header are present')
def verify_asset_col_header(context):
    ''' Verify asset column header '''
    time.sleep(5)
    try:
        WebDriverWait(context.browser, 60).until(
            EC.visibility_of_element_located((By.XPATH, asset_po.MAC_HEADER_XPATH))
        )
        asset_po.verify_asset_header(context)
        asset_po.verify_type_header(context)
        asset_po.verify_vendor_header(context)
        asset_po.verify_mac_header(context)
        asset_po.verify_ip_header(context)
        asset_po.verify_domain_header(context)
        asset_po.verify_hostname_header(context)
        asset_po.verify_networks_header(context)
        asset_po.verify_zone_header(context)
        asset_po.verify_created_at_header(context)
        asset_po.assertion_passed(context)
    except (TimeoutException, NoSuchElementException, AssertionError):
        asset_po.assertion_failed(context)


@then('I select a network')
def select_network(context):
    ''' Select a network '''
    time.sleep(1)
    try:
        WebDriverWait(context.browser, 20).until(
            EC.visibility_of_element_located((By.XPATH, asset_po.NETWORK_LIST_XPATH))
        )
        select_all = asset_po.get_asset_network_select_all_button(context)
        select_all.click()
        time.sleep(2)
        save_and_reload_button = asset_po.get_asset_select_network_save_button(context)
        save_and_reload_button.click()
        time.sleep(5)
        asset_po.assertion_passed(context)
    except (TimeoutException, NoSuchElementException, AssertionError):
        asset_po.assertion_failed(context)


@when('I click on the zones tab')
def click_zones_tab(context):
    ''' Zones tab '''
    try:
        WebDriverWait(context.browser, 20).until(
            EC.visibility_of_element_located((By.ID, asset_po.ZONES_TAB_ID))
        )
        zones_tab = asset_po.get_asset_zone_tab(context)
        zones_tab.click()
        zones_tab.click()
        time.sleep(1)
        asset_po.assertion_passed(context)
    except (TimeoutException, NoSuchElementException, AssertionError):
        asset_po.assertion_failed(context)


@then('the correct Zone column headers are present')
def verify_zone_col_header(context):
    ''' Verify zone column header '''
    try:
        WebDriverWait(context.browser, 20).until(
            EC.visibility_of_element_located((By.XPATH, asset_po.ZONE_COLUMN_HEADER_XPATH))
        )
        asset_po.verify_zone_name_column_header(context)
        asset_po.verify_asset_criteria_column_header(context)
        asset_po.assertion_passed(context)
    except (TimeoutException, NoSuchElementException, AssertionError):
        asset_po.assertion_failed(context)


@when('I click on create a new zone')
def create_new_zone(context):
    ''' Create new zone '''
    try:
        new_zone_button = asset_po.get_asset_new_zone_button(context)
        new_zone_button.click()
        asset_po.assertion_passed(context)
    except (NoSuchElementException, AssertionError):
        asset_po.assertion_failed(context)


@then('the create a new zone popup appears')
def verify_new_zone_popup(context):
    ''' Verify new zone popup '''
    try:
        time.sleep(1)
        create_new_zone_popup = asset_po.get_asset_create_new_zone_popup(context)
        assert create_new_zone_popup.is_displayed()
        asset_po.assertion_passed(context)
    except (NoSuchElementException, AssertionError):
        asset_po.assertion_failed(context)


@then('I click the zones refresh button')
def zones_refresh(context):
    ''' Zones refresh '''
    try:
        refresh_button = asset_po.get_zones_refresh_button(context)
        refresh_button.click()
        asset_po.assertion_passed(context)
    except (NoSuchElementException, AssertionError):
        asset_po.assertion_failed(context)


@then('I click the asset refresh button')
def asset_refresh(context):
    ''' Refresh assets '''
    try:
        time.sleep(5)
        WebDriverWait(context.browser, 60).until(EC.presence_of_element_located((By.XPATH,
                                                                                 asset_po.ASSET_REFRESH_BUTTON_XPATH)))
        refresh_button = asset_po.get_asset_refresh_button(context)
        refresh_button.click()
        time.sleep(5)
        asset_po.assertion_passed(context)
    except (TimeoutException, NoSuchElementException, AssertionError):
        asset_po.assertion_failed(context)


@when('I click the zone export button')
def click_zone_export_button(context):
    ''' Export zones '''
    try:
        zone_export_button = asset_po.get_zone_export_button(context)
        zone_export_button.click()
        asset_po.assertion_passed(context)
    except (NoSuchElementException, AssertionError):
        asset_po.assertion_failed(context)


@then('the export popup appears')
def verify_export_popup(context):
    ''' Verify export popup '''
    try:
        time.sleep(1)
        export_popup = asset_po.get_export_popup(context)
        assert export_popup.is_displayed()
        asset_po.assertion_passed(context)
    except (NoSuchElementException, AssertionError):
        asset_po.assertion_failed(context)
