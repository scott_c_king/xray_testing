#!/usr/bin/env python3

'''
Dragos QFD Steps Module
'''

import time
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from behave import when, then  # pylint: disable=no-name-in-module
import _support.page_objects.qfd_page_object as qfd_po


QFD_BACK_BUTTON_ID = 'back-button'
QFD_EXPORT_BUTTON_ID = 'open-export-modal__button'
QFD_EXPORT_MODULE_ID = 'export-modal__content'
QFD_COLUMN_PICKER_POPUP_ID = 'column-picker-modal__content'


@then('the page has export, back, and column filter buttons')
def has_correct_buttons(context):
    ''' Page has correct buttons '''
    try:
        time.sleep(1)
        qfd_back_button = context.browser.find_element(By.ID, QFD_BACK_BUTTON_ID)
        qfd_export_button = qfd_po.get_qfd_export_button(context)
        qfd_column_picker_button = qfd_po.get_qfd_column_picker_button(context)
        assert qfd_back_button.is_displayed()
        assert qfd_export_button.is_displayed()
        assert qfd_column_picker_button.is_displayed()
        qfd_po.assertion_passed(context)
    except (NoSuchElementException, AssertionError):
        qfd_po.assertion_failed(context)


@when('I click the qfd export button')
def click_qfd_export_button(context):
    ''' Click QFD export button '''
    try:
        time.sleep(1)
        qfd_export_button = qfd_po.get_qfd_export_button(context)
        qfd_export_button.click()
        qfd_po.assertion_passed(context)
    except (NoSuchElementException, AssertionError):
        qfd_po.assertion_failed(context)


@then('the qfd export popup appears')
def qfd_export_popup_appears(context):
    ''' QFD export popup appears '''
    try:
        time.sleep(.5)
        qfd_export_popup = context.browser.find_element(By.ID, QFD_EXPORT_MODULE_ID)
        assert qfd_export_popup.is_displayed()
        qfd_po.assertion_passed(context)
    except (NoSuchElementException, AssertionError):
        qfd_po.assertion_failed(context)


@when('I click the QFD column picker')
def click_qfd_column_picker(context):
    ''' Click QFD column picker '''
    try:
        qfd_column_picker_button = qfd_po.get_qfd_column_picker_button(context)
        qfd_column_picker_button.click()
        qfd_po.assertion_passed(context)
    except (NoSuchElementException, AssertionError):
        qfd_po.assertion_failed(context)


@then('the qfd column picker appears')
def qfd_column_picker_appears(context):
    ''' QFD column picker appears '''
    try:
        time.sleep(.5)
        qfd_column_picker_popup = context.browser.find_element(By.ID, QFD_COLUMN_PICKER_POPUP_ID)
        assert qfd_column_picker_popup.is_displayed()
        qfd_po.assertion_passed(context)
    except (NoSuchElementException, AssertionError):
        qfd_po.assertion_failed(context)


@when('I click the QFD back button')
def click_qfd_back_button(context):
    ''' Click QFD back button '''
    try:
        qfd_back_button = context.browser.find_element(By.ID, QFD_EXPORT_BUTTON_ID)
        qfd_back_button.click()
        qfd_po.assertion_passed(context)
    except (NoSuchElementException, AssertionError):
        qfd_po.assertion_failed(context)


@when('I click the csv/tsv toggle')
def click_csv_tsv_toggle(context):
    ''' Click CSV/TSV toggle '''
    try:
        toggle = qfd_po.get_qfd_export_toggle_to_click(context)
        toggle.click()
        qfd_po.assertion_passed(context)
    except (NoSuchElementException, AssertionError):
        qfd_po.assertion_failed(context)


@then('the toggle toggles')
def verify_toggle(context):
    ''' Verify toggle '''
    try:
        toggle = qfd_po.get_qfd_export_toggle(context)
        assert toggle.is_selected()
        qfd_po.assertion_passed(context)
    except (NoSuchElementException, AssertionError):
        qfd_po.assertion_failed(context)


@when('a qfd export column name checkbox is checked')
def qfd_export_checkbox(context):
    ''' QFD exoirt column name checkbox is checked '''
    try:
        time.sleep(.5)
        checkbox = qfd_po.get_qfd_export_column_name_checkbox(context)
        if not checkbox.is_selected():
            checkbox.click()
        qfd_po.assertion_passed(context)
    except (NoSuchElementException, AssertionError):
        qfd_po.assertion_failed(context)


@when('I click the export column name checkbox')
def click_export_column_checkbox(context):
    ''' Click export column checkbox '''
    try:
        checkbox = qfd_po.get_qfd_export_column_name_checkbox(context)
        checkbox.click()
        qfd_po.assertion_passed(context)
    except (NoSuchElementException, AssertionError):
        qfd_po.assertion_failed(context)


@then('Then the checkbox is not checked')
def checkbox_not_checked(context):
    ''' Checkbox is not checked '''
    try:
        checkbox = qfd_po.get_qfd_export_column_name_checkbox(context)
        is_the_checkbox_selected = checkbox.is_selected()
        assert not is_the_checkbox_selected
        qfd_po.assertion_passed(context)
    except (NoSuchElementException, AssertionError):
        qfd_po.assertion_failed(context)
