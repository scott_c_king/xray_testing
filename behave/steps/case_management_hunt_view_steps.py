#!/usr/bin/env python3

'''
Dragos Case Management Hunt View Steps Module
'''

import time
from selenium.common.exceptions import NoSuchElementException
from behave import when, then  # pylint: disable=no-name-in-module
import _support.page_objects.case_management_hunt_view_page_object as cmhv_po


HEADER_CLASS_NAME = 'name-header'


@then('I am on the test cases journal')
def on_test_cases_journal(context):
    ''' Verify on the test case page '''
    cmhv_po.verify_on_the_test_case_page(context)


@when('I click the journal placeholder')
def click_journal_placeholder(context):
    ''' Click journal field placeholder '''
    try:
        journal_field_placeholder = cmhv_po.get_journal_field_placeholder(context)
        journal_field_placeholder.click()
        cmhv_po.assertion_passed(context)
    except (NoSuchElementException, AssertionError):
        cmhv_po.assertion_failed(context)


@then('the journal editor appears')
def journal_editor_appears(context):
    ''' Journal editor '''
    try:
        time.sleep(1)
        journal_editor = cmhv_po.get_journal_editor(context)
        assert journal_editor.is_displayed()
        cmhv_po.assertion_passed(context)
    except (NoSuchElementException, AssertionError):
        cmhv_po.assertion_failed(context)


@when('I type information in the journal')
def journal_entry(context):
    ''' Test journal entry '''
    try:
        journal_editor = cmhv_po.get_journal_editor(context)
        journal_editor.click()
        journal_editor.clear()
        cmhv_po.send_keys_to_journal(journal_editor)
        submit_button = cmhv_po.get_journal_submit_button(context)
        submit_button.click()
        cmhv_po.assertion_passed(context)
    except (NoSuchElementException, AssertionError):
        cmhv_po.assertion_failed(context)


@then('the entry appears in the journal')
def verify_journal_entry(context):
    ''' Verify journal entry '''
    try:
        cmhv_po.verify_journal_entry_is_present(context)
        cmhv_po.assertion_passed(context)
    except (NoSuchElementException, AssertionError):
        cmhv_po.assertion_failed(context)
