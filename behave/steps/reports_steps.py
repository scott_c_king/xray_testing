#!/usr/bin/env python3

'''
Dragos Reports Steps Module
'''

import time
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from selenium.common.exceptions import NoSuchElementException
from behave import then, when  # pylint: disable=no-name-in-module
import _support.page_objects.reports_page_object as reports_po


REPORTS_NAME_HEADER_ID = 'report-name__table-header'


@then('the reports table has the correct columns')
def reports_table_correct(context):
    ''' Reports table has correct columns '''
    try:
        WebDriverWait(context.browser, 60).until(EC.visibility_of_element_located((By.ID, REPORTS_NAME_HEADER_ID)))
        reports_po.verify_reports_date_created_column_header(context)
        reports_po.verify_reports_name_column_header(context)
        reports_po.verify_reports_report_type_column_header(context)
        reports_po.assertion_passed(context)
    except (TimeoutException, NoSuchElementException, AssertionError):
        reports_po.assertion_failed(context)


@then('the correct buttons are there')
def buttons_are_correct(context):
    ''' Buttons are correct '''
    try:
        WebDriverWait(context.browser, 60).until(EC.visibility_of_element_located((By.ID, REPORTS_NAME_HEADER_ID)))
        reports_po.verify_reports_export_button(context)
        reports_po.verify_reports_create_new_button(context)
        reports_po.verify_reports_report_column_filter(context)
        reports_po.assertion_passed(context)
    except (TimeoutException, NoSuchElementException, AssertionError):
        reports_po.assertion_failed(context)


@when('I click create report')
def click_create_report(context):
    ''' Click create report '''
    try:
        create_report_button = reports_po.get_create_new_report_button(context)
        create_report_button.click()
        reports_po.assertion_passed(context)
    except (NoSuchElementException, AssertionError):
        reports_po.assertion_failed(context)


@then('the create new report popup appears')
def create_new_report_popup_appears(context):
    ''' Verify create new report popup appears '''
    try:
        time.sleep(1)
        create_new_report_popup = reports_po.get_create_new_report_popup(context)
        assert create_new_report_popup.is_displayed()
        reports_po.assertion_passed(context)
    except (NoSuchElementException, AssertionError):
        reports_po.assertion_failed(context)


@when('I click the export report button')
def click_export_report_button(context):
    ''' Click export report button '''
    try:
        export_report_button = reports_po.get_export_report_button(context)
        export_report_button.click()
        reports_po.assertion_passed(context)
    except (NoSuchElementException, AssertionError):
        reports_po.assertion_failed(context)


@then('the export report popup appears')
def export_report_popup_appears(context):
    ''' Export report popup appears '''
    try:
        time.sleep(1)
        export_popup = reports_po.get_export_popup(context)
        assert export_popup.is_displayed()
        reports_po.assertion_passed(context)
    except (NoSuchElementException, AssertionError):
        reports_po.assertion_failed(context)


@when('I click the report column picker button')
def click_report_column_picker_button(context):
    ''' Click report column picker button '''
    try:
        column_picker_button = reports_po.get_column_filter_button(context)
        column_picker_button.click()
        reports_po.assertion_passed(context)
    except (NoSuchElementException, AssertionError):
        reports_po.assertion_failed(context)


@then('the report column picker popup appears')
def report_column_picker_popup_appears(context):
    ''' Report column picker popup appears '''
    try:
        column_picker_popup = reports_po.get_column_filter_popup(context)
        assert column_picker_popup.is_displayed()
        reports_po.assertion_passed(context)
    except (NoSuchElementException, AssertionError):
        reports_po.assertion_failed(context)
