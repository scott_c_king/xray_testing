#!/usr/bin/env python3

'''
Dragos Content Steps Module
'''

import time
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from selenium.common.exceptions import NoSuchElementException
from behave import when, then  # pylint: disable=no-name-in-module
import _support.page_objects.content_page_object as content_po


CONTENT_CONT_ID = 'analytic-manager-tabs'
DETECTIONS_TAB_ID = 'tabs-detection-0'
CHAR_TAB_ID = 'tabs-characterization-1'
CREATE_NEW_ANALYTIC_POPUP_ID = 'new-analytic-modal'
CHAR_TAB_INDICATOR_XPATH = '//*[@id="tabs-characterization-1"]/div'
CHAR_NEW_ANALYTIC_POPUP_XPATH = ('/html/body/div[1]/div/div/div/div[4]/div/div/div/div/'
                                 'div[2]/div/div[1]/div[1]/div[2]/div[5]/div/div[2]/div[1]')


@then('the content page loads')
def content_page_loads(context):
    ''' Load content page '''
    try:
        content_container = context.browser.find_element(By.ID, CONTENT_CONT_ID)
        assert content_container.is_displayed()
        content_po.assertion_passed(context)
    except (NoSuchElementException, AssertionError):
        content_po.assertion_failed(context)


@then('the detections and characterization tabs are present')
def tabs_are_present(context):
    ''' Verify detections and characterization tabs are present '''
    try:
        assert context.browser.find_element(By.ID, DETECTIONS_TAB_ID).is_displayed()
        assert context.browser.find_element(By.ID, CHAR_TAB_ID).is_displayed()
        content_po.assertion_passed(context)
    except (NoSuchElementException, AssertionError):
        content_po.assertion_failed(context)


@then('the correct character-detections items are present')
def char_detections_present(context):
    ''' Characte detection items are present '''
    try:
        time.sleep(1)
        WebDriverWait(context.browser, 60).until(
            EC.presence_of_element_located((By.XPATH, content_po.GROUP_BY_BUTTON_XPATH))
        )
        group_by_button = content_po.get_char_group_by_button(context)
        dragos_toggle = content_po.get_char_dragos_toggle(context)
        user_analytics_button = content_po.get_char_user_analytics_button(context)
        play_button = content_po.get_char_play_button(context)
        pause_button = content_po.get_char_pause_button(context)
        plus_analytic_button = content_po.get_char_analytic_button(context)

        assert group_by_button.is_displayed()
        assert dragos_toggle.is_displayed()
        assert user_analytics_button.is_displayed()
        assert play_button.is_displayed()
        assert pause_button.is_displayed()
        assert plus_analytic_button.is_displayed()
        content_po.assertion_passed(context)
    except (TimeoutException, NoSuchElementException, AssertionError):
        content_po.assertion_failed(context)


@then('the correct content-detections items are present')
def content_detections_present(context):
    ''' Content detections present '''
    time.sleep(1)
    try:
        WebDriverWait(context.browser, 60).until(
            EC.presence_of_element_located((By.XPATH, content_po.GROUP_BY_BUTTON_XPATH))
        )
        group_by_button = content_po.get_group_by_button(context)
        dragos_toggle = content_po.get_dragos_toggle(context)
        user_analytics_button = content_po.get_user_analytics_button(context)
        play_button = content_po.get_play_button(context)
        pause_button = content_po.get_pause_button(context)
        plus_analytic_button = content_po.get_analytic_button(context)

        assert group_by_button.is_displayed()
        assert dragos_toggle.is_displayed()
        assert user_analytics_button.is_displayed()
        assert play_button.is_displayed()
        assert pause_button.is_displayed()
        assert plus_analytic_button.is_displayed()
        content_po.assertion_passed(context)
    except (TimeoutException, NoSuchElementException, AssertionError):
        content_po.assertion_failed(context)


@when('I click the + ANALYTIC button')
def click_analytic_button(context):
    ''' Click analytic button '''
    try:
        time.sleep(1)
        plus_analytic = content_po.get_analytic_button(context)
        plus_analytic.click()
        content_po.assertion_passed(context)
    except (NoSuchElementException, AssertionError):
        content_po.assertion_failed(context)


@when('I click the Char + ANALYTIC button')
def click_char_analytic_button(context):
    ''' Click char analytic button '''
    try:
        time.sleep(1)
        plus_analytic = content_po.get_char_analytic_button(context)
        plus_analytic.click()
        content_po.assertion_passed(context)
    except (NoSuchElementException, AssertionError):
        content_po.assertion_failed(context)


@then('the create new analytic popup appears')
def new_analytic_popup_appears(context):
    ''' New analytic popup appears '''
    try:
        WebDriverWait(context.browser, 60).until(EC.presence_of_element_located((By.ID, CREATE_NEW_ANALYTIC_POPUP_ID)))
        create_new_analytic_popup = content_po.get_create_new_analytic_popup(context)
        assert create_new_analytic_popup.is_displayed()
        content_po.assertion_passed(context)
    except (TimeoutException, NoSuchElementException, AssertionError):
        content_po.assertion_failed(context)


@when('I click the characterization tab')
def click_characterization_tab(context):
    ''' Click characterization tab '''
    try:
        char_tab = content_po.get_char_tab(context)
        char_tab.click()
        content_po.assertion_passed(context)
    except (NoSuchElementException, AssertionError):
        content_po.assertion_failed(context)


@then('I am on the content characterization tab')
def on_content_characterization_tab(context):
    ''' Verify on content characterization tab '''
    try:
        char_tab_indicator = context.browser.find_element(By.XPATH, CHAR_TAB_INDICATOR_XPATH)
        char_tab_class = char_tab_indicator.get_attribute("class")
        print(char_tab_class)
        assert char_tab_class == 'active-indicator'
        content_po.assertion_passed(context)
    except (NoSuchElementException, AssertionError):
        content_po.assertion_failed(context)


@then('the create new char analytic popup appears')
def create_new_char_analytic_popup(context):
    ''' Create new char analytic popup '''
    try:
        WebDriverWait(context.browser, 10).until(
            EC.presence_of_element_located((By.XPATH, CHAR_NEW_ANALYTIC_POPUP_XPATH))
        )
        new_char_analytic_popup = context.browser.find_element(By.XPATH, CHAR_NEW_ANALYTIC_POPUP_XPATH)
        assert new_char_analytic_popup.is_displayed()
        content_po.assertion_passed(context)
    except (TimeoutException, NoSuchElementException, AssertionError):
        content_po.assertion_failed(context)
