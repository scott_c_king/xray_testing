#!/usr/bin/env python3

'''
Dragos Util Steps Module
'''

import time
from behave import given, when, then  # pylint: disable=no-name-in-module
import _support.page_objects.util_page_object as util_po
import _support.page_objects.login_page as login_po
import _support.page_objects.detections_dashboard_page_object as dd_po
import _support.page_objects.navigation_page_object as nav_po


ASSETS_BUTTON_XPATH = '//*[@id="primary-nav__upper__icon-links__assetexplorer"]/a'
REPORTS_BUTTON_XPATH = '//*[@id="primary-nav__upper__icon-links__reportmanager"]'
DATA_BUTTON_XPATH = '//*[@id="primary-nav__upper__icon-links__dataexplorer"]'
QFD_LAUNCH_BUTTON_XPATH = '//*[@id="root"]/div/div/div/div/div[4]/div/div/div/div/div[1]/div/div[2]/div[2]/button'
TM_LAUNCH_BUTTON_XPATH = '//*[@id="root"]/div/div/div/div/div[4]/div/div/div/div/div[3]/div/div[2]/div[2]/button'
FILE_MANAGER_LAUNCH_BUTTON_XPATH = '//*[@id="root"]/div/div/div/div[4]/div/div/div/div/div[2]/div/div[2]/div[2]/button'
CONTENT_BUTTON_XPATH = '//*[@id="primary-nav__upper__icon-links__contentmanager"]/a/span'
SENSORS_BUTTON_ID = 'primary-nav__upper__icon-links__sensors'
NOTIFICATION_BUTTON_XPATH = '//*[@id="primary-nav__upper__icon-links__notificationmanager"]/a'


@when('I close the browser')
def close_the_browser(context):
    ''' Close the browser '''
    context.browser.quit()


@when('I logout')
def logout(context):
    ''' Log out of the platform '''
    util_po.logout_of_the_platform(context)


@then('I am on the "(.*)" tab')
def on_the_correct_tab(context, app_side):
    ''' Find which side of app is open '''
    util_po.find_which_side_of_app_is_open(context, app_side)


@then('I wait for 1 second')
def wait_for(context):
    time.sleep(1)


@then('I wait for "(.*)" seconds')
def wait_for(context, seconds):
    time.sleep(int(seconds))


@given('I am on the detections dashboard')
def on_the_detections_dashboard(context):
    ''' On the detections dashboard '''
    login_po.nav_to_platform(context)
    login_po.login_with_valid_credentials(context)
    dd_po.click_on_full_slide_arrow(context, 'right')


@given('I am on the assets page')
def on_the_assets_page(context):
    ''' On the assets page '''
    login_po.nav_to_platform(context)
    login_po.login_with_valid_credentials(context)
    nav_po.click_a_button(context, ASSETS_BUTTON_XPATH)


@given('I am on the reports page')
def on_the_reports_page(context):
    ''' On the reports page '''
    login_po.nav_to_platform(context)
    login_po.login_with_valid_credentials(context)
    nav_po.click_a_button(context, REPORTS_BUTTON_XPATH)


@given('I am on the Data page')
def on_the_data_page(context):
    ''' On the data page '''
    login_po.nav_to_platform(context)
    login_po.login_with_valid_credentials(context)
    nav_po.click_a_button(context, DATA_BUTTON_XPATH)


@given('I am on the QFD page')
def on_the_qfd_page(context):
    ''' On the QFD page '''
    login_po.nav_to_platform(context)
    login_po.login_with_valid_credentials(context)
    nav_po.click_a_button(context, DATA_BUTTON_XPATH)
    nav_po.click_a_button(context, QFD_LAUNCH_BUTTON_XPATH)


@given('I am on the tasking manager page')
def on_tasking_manager_page(context):
    ''' On the tasking manager page '''
    login_po.nav_to_platform(context)
    login_po.login_with_valid_credentials(context)
    nav_po.click_a_button(context, DATA_BUTTON_XPATH)
    nav_po.click_a_button(context, TM_LAUNCH_BUTTON_XPATH)


@given('I am on the file manager page')
def on_file_manager_page(context):
    ''' On the file manager page '''
    login_po.nav_to_platform(context)
    login_po.login_with_valid_credentials(context)
    nav_po.click_a_button(context, DATA_BUTTON_XPATH)
    nav_po.click_a_button(context, FILE_MANAGER_LAUNCH_BUTTON_XPATH)


@given('I am on the content page')
def on_the_content_page(context):
    ''' On the content page '''
    login_po.nav_to_platform(context)
    login_po.login_with_valid_credentials(context)
    nav_po.click_a_button(context, CONTENT_BUTTON_XPATH)


@given('I nav to the network sensor tab')
def on_the_network_sensor_tab(context):
    ''' Nav to the network sensor tab '''
    login_po.nav_to_platform(context)
    login_po.login_with_valid_credentials(context)
    nav_po.click_a_button(context, SENSORS_BUTTON_ID)


@when('I navigate to the notifications page')
def on_the_notifications_page(context):
    ''' On the notifications page '''
    nav_po.click_a_button(context, NOTIFICATION_BUTTON_XPATH)
