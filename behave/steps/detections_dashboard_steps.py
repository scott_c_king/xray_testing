#!/usr/bin/env python3

'''
Dragos Detection Dashboard Steps Module
'''

import time
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from behave import when, then  # pylint: disable=no-name-in-module
import _support.page_objects.detections_dashboard_page_object as dd_po


MODULE_DICT = {
    'modeling': '//*[@id="root"]/div/div/div/div/div[2]/div/div/div[2]/div[1]/div',
    'threat': '//*[@id="root"]/div/div/div/div/div[2]/div/div/div[2]/div[2]',
    'config': '//*[@id="root"]/div/div/div/div/div[2]/div/div/div[2]/div[3]',
    'indicator': '//*[@id="root"]/div/div/div/div/div[2]/div/div/div[2]/div[4]'}
MODELING_DETECTION_GRAPH_XPATH = '/html/body/div[1]/div/div/div/div[2]/div/div/div[2]/div[1]/div'
RIGHT_MODELING_GRAPH_ARROW_XPATH = ('//*[@id="detection-dashboard"]/div[2]/div[1]/div/div/div[1]/'
                                    'div[2]/div/div[4]/div/i')
MODELING_GRAPH_BAR_CHART_XPATH = ('//*[@id="detection-dashboard"]/div[2]/div[1]/div/div/div[1]/'
                                  'div[2]/div/div[2]/div/div[2]/div')
MODELING_POPUP_XPATH = '//*[@id="modal__content"]/div'
DNP3_LINK_LAYER_NACK_ERROR = "//div[contains(text(), 'DNP3 Link Layer NACK Error Detected')]"
DETECTIONS_SUMMARY_FIELD_XPATH = '//*[@id="modal__content"]/div/div[2]/div/div[1]/div[1]/div/div[3]/div'
DN3P_NACK_DETECTED_BY_FIELD_XPATH = '//*[@id="qualified-analytics"]'
DN3P_NACK_NOTIFICATION_TITLE_XPATH = '//*[@id="modal__content"]/div/div[1]/div[2]/div[2]'
DN3P_NACK_NOTIFICATION_TITLE_TEXT = 'DNP3 Link Layer NACK Error Detected'
DN3P_NACK_NOTIFICATION_SUMMARY_INFO = ('Outstation asset 12274 (10.40.1.69) sent a Not Acknowledged (NACK) '
                                       'response to the master asset 12260 (192.168.123.69)')
DN3P_NACK_DETECTED_BY_TEXT = 'DNP3 Not Acknowledged Error'
DETECTIONS_GROUP_BY_DROPDOWN_ID = 'groupBy'
DETECTIONS_SELECT_UNGROUPED_XPATH = '//*[@id="groupBy__select-content"]/li[2]/span'
UNREAD_FILTER_VISIBLE_XPATH = ('//*[@id="notifications-manager__tabs"]/div/div[1]/div/div/div/div[1]/'
                               'div[3]/div/span[2]/div[2]/div')


@then('The "(.*)" module is present')
def module_is_present(context, module_name):
    ''' Module is present '''
    dd_po.is_module_present(context, MODULE_DICT[module_name])


@then('The modules have correct titles')
def modules_correct_titles(context):
    ''' Modules have correct titles '''
    dd_po.assert_module_names(context)


@when('I click the "(.*)" full-slide arrow')
def full_slide_arrow(context, arrow_side):
    ''' Click the full slide arrow '''
    dd_po.click_on_full_slide_arrow(context, arrow_side)


@then('the "(.*)" bar graph shows')
def bar_graph_shows(context, graph):
    ''' Bar graph '''
    dd_po.did_the_graph_change_to_the_bar_graph(context, graph)


@then('the "(.*)" has 20 items per page')
def items_per_page(context, notification_list_css):
    ''' Items per page '''
    time.sleep(2)
    dd_po.assert_how_many_items_are_in_notification_list_page(context, notification_list_css)


@when('I click a notification')
def click_notification(context):
    ''' Click notification '''
    dd_po.click_the_first_notification(context)


@then('the pop-up appears')
def popup_appears(context):
    ''' Popup appears '''
    dd_po.assert_popup_appeared(context)


@then('the notification popup has the correct fields')
def notification_popup_correct(context):
    ''' Notification popup appears '''
    dd_po.verify_notification_popup_fields(context)


@when('I click the "create a case" button')
def click_create_a_case(context):
    ''' Click create a case '''
    try:
        create_a_case_button = dd_po.find_create_a_case_button(context)
        create_a_case_button.click()
        dd_po.assertion_passed(context)
    except (NoSuchElementException, AssertionError):
        dd_po.assertion_failed(context)


@then('the "Create a Case" module appears')
def create_a_case_appears(context):
    ''' Creat a case appears '''
    dd_po.assert_create_test_case_appears(context)


@when('I hover over the modeling information icon')
def hover_over_modeling_info(context):
    ''' Hover over modeling info '''
    try:
        time.sleep(1)
        modeling_information_icon = dd_po.get_information_icon(context)
        modeling_information_icon[0].click()
        dd_po.assertion_passed(context)
    except (NoSuchElementException, IndexError, AssertionError):
        dd_po.assertion_failed(context)


@when('I hover over the threat behavior information icon')
def hover_over_threat_behavior(context):
    ''' Hover over threat behavior '''
    try:
        time.sleep(1)
        threat_behavior_information_icon = dd_po.get_information_icon(context)
        threat_behavior_information_icon[1].click()
        dd_po.assertion_passed(context)
    except (NoSuchElementException, IndexError, AssertionError):
        dd_po.assertion_failed(context)


@when('I hover over the configuration information icon')
def hover_over_config_info(context):
    ''' Hover over config info '''
    try:
        time.sleep(1)
        threat_behavior_information_icon = dd_po.get_information_icon(context)
        threat_behavior_information_icon[2].click()
        dd_po.assertion_passed(context)
    except (NoSuchElementException, IndexError, AssertionError):
        dd_po.assertion_failed(context)


@when('I hover over the indicator information icon')
def hover_over_indicator_info(context):
    ''' Hover over indicator info '''
    try:
        time.sleep(1)
        threat_behavior_information_icon = dd_po.get_information_icon(context)
        threat_behavior_information_icon[3].click()
        dd_po.assertion_passed(context)
    except (NoSuchElementException, IndexError, AssertionError):
        dd_po.assertion_failed(context)


@then('the modeling information text appears')
def modeling_info_text(context):
    ''' Modeling info text '''
    try:
        information_text = dd_po.get_information_text(context)
        # makes sure that active in the class list
        is_active = "active" in information_text[0].get_attribute("class")
        assert is_active
        dd_po.assertion_passed(context)
    except (NoSuchElementException, IndexError, AssertionError):
        dd_po.assertion_failed(context)


@then('the threat behavior information text appears')
def threat_behavior_info_appears(context):
    ''' threat behavior info appears '''
    try:
        information_text = dd_po.get_information_text(context)
        # makes sure that active in the class list
        is_active = "active" in information_text[1].get_attribute("class")
        assert is_active
        dd_po.assertion_passed(context)
    except (NoSuchElementException, IndexError, AssertionError):
        dd_po.assertion_failed(context)


@then('the configuration information text appears')
def config_info_appears(context):
    ''' Config info appears '''
    try:
        information_text = dd_po.get_information_text(context)
        # makes sure that active in the class list
        is_active = "active" in information_text[2].get_attribute("class")
        assert is_active
        dd_po.assertion_passed(context)
    except (NoSuchElementException, IndexError, AssertionError):
        dd_po.assertion_failed(context)


@then('the indicator information text appears')
def indicator_info_appears(context):
    ''' Indicator info appears '''
    try:
        information_text = dd_po.get_information_text(context)
        # makes sure that active in the class list
        is_active = "active" in information_text[3].get_attribute("class")
        assert is_active
        dd_po.assertion_passed(context)
    except (NoSuchElementException, IndexError, AssertionError):
        dd_po.assertion_failed(context)


@when('I click the modeling detections notification ...')
def click_modeling_detections(context):
    ''' Click modeling detections '''
    try:
        time.sleep(.5)
        notification_menu_button = dd_po.get_modeling_notification_button(context)
        notification_menu_button.click()
        dd_po.assertion_passed(context)
    except (NoSuchElementException, AssertionError):
        dd_po.assertion_failed(context)


@then('the modeling detections submenu appears')
def modeling_detections_submenu(context):
    ''' Modeling detections submenu '''
    try:
        modeling_notification_menu = dd_po.get_modeling_notification_menu(context)
        is_active = "active" in modeling_notification_menu.get_attribute("class")
        print(is_active)
        assert is_active
        dd_po.assertion_passed(context)
    except (NoSuchElementException, AssertionError):
        dd_po.assertion_failed(context)


@then('the detections modeling graph is visible')
def detections_modeling_graph(context):
    ''' Detections modeling graph '''
    try:
        modeling_notification_graph = context.browser.find_element(By.XPATH, MODELING_DETECTION_GRAPH_XPATH)
        assert modeling_notification_graph.is_displayed()
        dd_po.assertion_passed(context)
    except (NoSuchElementException, AssertionError):
        dd_po.assertion_failed(context)


# Haley clean this the fuck up
# seriously don't leave this forever like this
@when('I set the date to include all of January 2020')
def set_the_date(context):
    ''' Set the date '''
    # click the From Field
    try:
        time.sleep(2)
        detections_date_picker_from_field = dd_po.get_detections_dashboard_from_field(context)
        detections_date_picker_from_field.click()
        time.sleep(1)
        # find month value
        detections_date_picker_month_value = dd_po.get_detections_dashboard_from_current_month_value(context)
        # Find back arrow
        detections_date_picker_back_month_arrow = dd_po.get_detections_calendar_back_arrow(context)
        # hit the back arrow until the month == Jan
        while detections_date_picker_month_value != 'January':
            detections_date_picker_back_month_arrow.click()
            time.sleep(1)
            detections_date_picker_month_value = dd_po.get_detections_dashboard_from_current_month_value(context)
        # click '1'
        # this is bad. Find a better way
        detections_date_picker_first_of_month = context.browser.find_element(By.XPATH,
                                                                             dd_po.DETECTIONS_DATE_PICKER_JAN1_XPATH)
        detections_date_picker_first_of_month.click()
        time.sleep(1)
        dd_po.assertion_passed(context)
    except (NoSuchElementException, AssertionError):
        dd_po.assertion_failed(context)


@then('the detections dashboard date range changes')
def detections_dashboard_rate(context):
    ''' Detections dashboard rate '''
    try:
        time.sleep(2)
        detections_date_picker_from_field = dd_po.get_detections_dashboard_from_field(context)
        detections_date_picker_from_field.click()
        time.sleep(1)
        # find month value
        # to find date of date picker text try .get_property("value") or .get_attribute('value')
        detections_date_picker_month_value = dd_po.get_detections_dashboard_from_current_month_value(context)
        assert detections_date_picker_month_value == 'January'
        dd_po.assertion_passed(context)
    except (NoSuchElementException, AssertionError):
        dd_po.assertion_failed(context)


@when('I click the right modeling graph arrow')
def right_modeling_graph(context):
    ''' Right modeling graph '''
    try:
        right_modeling_graph_arrow = context.browser.find_element(By.XPATH, RIGHT_MODELING_GRAPH_ARROW_XPATH)
        right_modeling_graph_arrow.click()
        dd_po.assertion_passed(context)
    except (NoSuchElementException, AssertionError):
        dd_po.assertion_failed(context)


@then('the severity bar graph is visible')
def severity_bar_is_visible(context):
    ''' Severity bar is visible '''
    try:
        time.sleep(1)
        context.browser.find_element(By.XPATH, MODELING_GRAPH_BAR_CHART_XPATH)
        dd_po.assertion_passed(context)
    except (NoSuchElementException, AssertionError):
        dd_po.assertion_failed(context)


@when('I click a DN3P detections modeling alert')
def dn3p_detections_modeling_alert(context):
    ''' DN3P detections modeling alert '''
    try:
        time.sleep(1)
        dn3p_modeling_alert = context.browser.find_element_by_xpath(DNP3_LINK_LAYER_NACK_ERROR)
        dn3p_modeling_alert.click()
        time.sleep(2)
        dd_po.assertion_passed(context)
    except (NoSuchElementException, AssertionError):
        dd_po.assertion_failed(context)


@then('the detections modeling alert popup appears')
def modeling_alert_popup_appears(context):
    ''' Modeling alert popup appears '''
    try:
        modeling_notification_popup = context.browser.find_element(By.XPATH, MODELING_POPUP_XPATH)
        assert modeling_notification_popup.is_displayed()
        dd_po.assertion_passed(context)
    except (NoSuchElementException, AssertionError):
        dd_po.assertion_failed(context)


@then('the detections modeling alert popup has the expected fields populated')
def modeling_popup_expected_fields(context):
    ''' Modeling popup expected fields '''
    # occured at == date format
    # title
    # source/dest ip = regex
    try:
        dn3p_notification_title = context.browser.find_element(By.XPATH, DN3P_NACK_NOTIFICATION_TITLE_XPATH).text
        # dn3p_modeling_det_summary_field_text = context.browser.find_element(
        #                                        By.XPATH, DETECTIONS_SUMMARY_FIELD_XPATH).text
        dn3p_nack_det_by_field_text_value = context.browser.find_element(By.XPATH,
                                                                         DN3P_NACK_DETECTED_BY_FIELD_XPATH).text
        print(DN3P_NACK_NOTIFICATION_SUMMARY_INFO)
        assert dn3p_notification_title == DN3P_NACK_NOTIFICATION_TITLE_TEXT
        # doesn't work, don't know why
        # assert dn3p_modeling_det_summary_field_text == DN3P_NACK_NOTIFICATION_SUMMARY_INFO
        assert dn3p_nack_det_by_field_text_value == DN3P_NACK_DETECTED_BY_TEXT
        dd_po.assertion_passed(context)
    except (NoSuchElementException, AssertionError):
        dd_po.assertion_failed(context)


@when('I set the group by to ungrouped')
def group_by_ungrouped(context):
    ''' Group by ungrouped '''
    try:
        # select dropdown
        detections_group_by_dropdown = context.browser.find_element(By.ID, DETECTIONS_GROUP_BY_DROPDOWN_ID)
        detections_group_by_dropdown.click()
        # select ungrouped
        detections_select_ungrouped = context.browser.find_element(By.XPATH, DETECTIONS_SELECT_UNGROUPED_XPATH)
        detections_select_ungrouped.click()
        time.sleep(1)
        dd_po.assertion_passed(context)
    except (NoSuchElementException, AssertionError):
        dd_po.assertion_failed(context)


@then('the status == unread filter is visible')
def unread_filter_is_visible(context):
    ''' Unread filter is visible '''
    try:
        unread_filter_visible = context.browser.find_element(By.XPATH, UNREAD_FILTER_VISIBLE_XPATH).text
        assert unread_filter_visible == 'Status==Unread'
        dd_po.assertion_passed(context)
    except (NoSuchElementException, AssertionError):
        dd_po.assertion_failed(context)

# ask for dev help
# @then('the detections page shows the ungrouped graphs')
# def step_imp(context):
