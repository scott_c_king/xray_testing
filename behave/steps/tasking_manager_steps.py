#!/usr/bin/env python3

'''
Dragos Tasking Manager Steps Module
'''

import time
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from behave import when, then  # pylint: disable=no-name-in-module
import _support.page_objects.qfd_page_object as qfd_po


TM_BACK_BUTTON_XPATH = '//*[@id="tasking-table-header"]/div[1]/div/button'
TM_EXPORT_BUTTON_ID = 'open-export-modal__button'
TM_EXPORT_MODULE_ID = 'export-modal__content'
TM_COLUMN_PICKER_POPUP_ID = 'column-picker-modal__content'
TM_COLUMN_PICKER_BUTTON_ID = 'column-picker__button'


@then('the tm page has export, back, and column filter buttons')
def tm_page_has_correct_buttons(context):
    ''' The TM page has correct buttons '''
    try:
        time.sleep(1)
        tm_back_button = context.browser.find_element(By.XPATH, TM_BACK_BUTTON_XPATH)
        tm_export_button = context.browser.find_element(By.ID, TM_EXPORT_BUTTON_ID)
        tm_column_picker_button = context.browser.find_element(By.ID, TM_COLUMN_PICKER_BUTTON_ID)
        assert tm_back_button.is_displayed()
        assert tm_export_button.is_displayed()
        assert tm_column_picker_button.is_displayed()
        qfd_po.assertion_passed(context)
    except (NoSuchElementException, AssertionError):
        qfd_po.assertion_failed(context)


@when('I click the tm export button')
def click_tm_export_button(context):
    ''' Click TM export button '''
    try:
        time.sleep(1)
        tm_export_button = context.browser.find_element(By.ID, TM_EXPORT_BUTTON_ID)
        tm_export_button.click()
        qfd_po.assertion_passed(context)
    except (NoSuchElementException, AssertionError):
        qfd_po.assertion_failed(context)


@then('the tm export popup appears')
def tm_export_popup_appears(context):
    ''' TM export popup appears '''
    try:
        time.sleep(.5)
        tm_export_popup = context.browser.find_element(By.ID, TM_EXPORT_MODULE_ID)
        assert tm_export_popup.is_displayed()
        qfd_po.assertion_passed(context)
    except (NoSuchElementException, AssertionError):
        qfd_po.assertion_failed(context)


@when('I click the tm column picker')
def click_tm_column_picker(context):
    ''' Click the TM column picker '''
    try:
        tm_column_picker_button = context.browser.find_element(By.ID, TM_COLUMN_PICKER_BUTTON_ID)
        tm_column_picker_button.click()
        qfd_po.assertion_passed(context)
    except (NoSuchElementException, AssertionError):
        qfd_po.assertion_failed(context)


@then('the tm column picker appears')
def tm_column_picker_appears(context):
    ''' TM column picker appears '''
    try:
        time.sleep(.5)
        tm_column_picker_popup = context.browser.find_element(By.ID, TM_COLUMN_PICKER_POPUP_ID)
        assert tm_column_picker_popup.is_displayed()
        qfd_po.assertion_passed(context)
    except (NoSuchElementException, AssertionError):
        qfd_po.assertion_failed(context)


@when('I click the tm back button')
def click_on_tm_back_button(context):
    ''' Click on the TM back button '''
    try:
        tm_back_button = context.browser.find_element(By.ID, TM_EXPORT_BUTTON_ID)
        tm_back_button.click()
        qfd_po.assertion_passed(context)
    except (NoSuchElementException, AssertionError):
        qfd_po.assertion_failed(context)
