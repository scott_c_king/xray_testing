#!/usr/bin/env python3

'''
Dragos Create and Edit Users Steps Module
'''

import time
from faker import Faker
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from behave import given, when, then  # pylint: disable=no-name-in-module
import _support.page_objects.navigation_page_object as nav_po
import _support.page_objects.login_page as login_po
import _support.page_objects.create_and_edit_user_page_object as caeu_po


FAKE = Faker()
VALID_USER_INFORMATION = FAKE.profile(fields=None, sex=None)
ADMIN_BUTTON_ON_APPS = '//*[@id="root"]/div/div/div/div[4]/div/div/div/div/div[9]/div/div[2]/div[2]/button'
CREATE_USER_XPATH = '//*[@id="table-header"]/div[1]/div/button'
PASSWORD_THAT_MEETS_THE_STUPID_REQUIREMENTS = 'thisIsAValidPassword1234568!'
SUBMIT_BUTTON_ID = 'save-create-user-modal__button'


@given('I am on the admin page')
def on_admin_page(context):
    ''' On admin page '''
    login_po.nav_to_platform(context)
    login_po.login_with_valid_credentials(context)
    nav_po.click_a_button(context, ADMIN_BUTTON_ON_APPS)


@when('I open the create user popup')
def open_create_user_popup(context):
    ''' Open create user popup '''
    time.sleep(1)
    nav_po.click_a_button(context, CREATE_USER_XPATH)


@when('I enter valid data')
def enter_valid_data(context):
    ''' Enter valid data '''
    try:
        id_field = caeu_po.get_id_field(context)
        id_field.send_keys(VALID_USER_INFORMATION['username'])
        email_address = caeu_po.get_email_field(context)
        email_address.send_keys(VALID_USER_INFORMATION['mail'])
        firstname_lastname = VALID_USER_INFORMATION['name'].split()
        first_name_field = caeu_po.get_firstname_field(context)
        first_name_field.send_keys(firstname_lastname[0])
        lastname_field = caeu_po.get_lastname_field(context)
        lastname_field.send_keys(firstname_lastname[1])
        create_user_password_field = caeu_po.get_password_field(context)
        create_user_confirm_password_field = caeu_po.get_confirm_password_field(context)
        create_user_password_field.send_keys(PASSWORD_THAT_MEETS_THE_STUPID_REQUIREMENTS)
        create_user_confirm_password_field.send_keys(PASSWORD_THAT_MEETS_THE_STUPID_REQUIREMENTS)
        context.browser.find_element(By.ID, SUBMIT_BUTTON_ID).click()
        caeu_po.assertion_passed(context)
    except (NoSuchElementException, AssertionError):
        caeu_po.assertion_failed(context)


@then("I am on the user's profile")
def on_user_profile(context):
    ''' On user profile '''
    try:
        user_profile = caeu_po.get_user_profile_module(context)
        caeu_po.verify_user_profile_is_present(context, user_profile)
        caeu_po.assertion_passed(context)
    except (NoSuchElementException, AssertionError):
        caeu_po.assertion_failed(context)
