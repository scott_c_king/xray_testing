#!/usr/bin/env python3

'''
Dragos Case Management Steps Module
'''

import time
from selenium.common.exceptions import NoSuchElementException
from behave import when, then  # pylint: disable=no-name-in-module
import _support.page_objects.detections_dashboard_page_object as dd_po
import _support.page_objects.navigation_page_object as nav_po
import _support.page_objects.case_management_hunt_view_page_object as cmhv_po


CREATE_CASE_SUBMIT_BUTTON_XPATH = ('/html/body/div/div/div/div/div/div[2]/div/div/div[2]/'
                                   'div[2]/div/div[2]/div[2]/div[2]/div[2]/a[1]')


@when('I enter valid information')
def enter_valid_info(context):
    ''' Enter valid info '''
    dd_po.send_valid_info(context)


@when('I click the submit button to create a case')
def click_submit_button(context):
    ''' Click submit button '''
    nav_po.click_a_button(context, CREATE_CASE_SUBMIT_BUTTON_XPATH)


@then('the test case has the information entered during creation')
def test_case_entry(context):
    ''' Verify test case entry '''
    try:
        time.sleep(2)
        test_case_title = cmhv_po.get_test_case_title(context)
        test_case_visibility = cmhv_po.get_test_case_visibility_field(context)
        test_case_priority = cmhv_po.get_test_case_priority(context)
        print(test_case_priority)
        # assert that the current title matches the title used to create the test case
        assert dd_po.TEST_CASE_NAME == test_case_title
        # test cases are currently  hardcoded to be created with a priority of 2
        # and public visibility
        assert test_case_visibility == 'PUBLIC'
        assert test_case_priority == '2'
        cmhv_po.assertion_passed(context)
    except (NoSuchElementException, AssertionError):
        cmhv_po.assertion_failed(context)
