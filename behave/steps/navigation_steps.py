#!/usr/bin/env python3

'''
Dragos Navigation Steps Module
'''


from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from selenium.common.exceptions import NoSuchElementException
from behave import when, then  # pylint: disable=no-name-in-module
import _support.page_objects.navigation_page_object as nav_po


NOTIFICATIONS_BUTTON = '//*[@id="primary-nav__upper__icon-links__notificationmanager"]'
REPORTS_BUTTON_XPATH = '//*[@id="primary-nav"]/div[1]/ul/li[9]/a/i'
ASSETS_BUTTON_XPATH = '//*[@id="primary-nav"]/div[1]/ul/li[3]'


@when('I click "(.*)"')
def click(context, button):
    ''' Click a button '''
    nav_po.click_a_button(context, button)


@then('the page class "(.*)" exists')
def page_class_exists(context, class_name):
    ''' Verify page class exists '''
    try:
        WebDriverWait(context.browser, 20).until(EC.visibility_of_element_located((By.CLASS_NAME, class_name)))
        nav_po.verify_page_class(context, class_name)
        nav_po.assertion_passed(context)
    except (TimeoutException, NoSuchElementException, AssertionError):
        nav_po.assertion_failed(context)


@when('I click Notifications')
def click_notification(context):
    ''' Notifications button '''
    nav_po.click_a_button(context, NOTIFICATIONS_BUTTON)


@when('I click Reports')
def click_reports(context):
    ''' Reports button '''
    nav_po.click_a_button(context, REPORTS_BUTTON_XPATH)


@when('I click on the asset button')
def click_asset_button(context):
    ''' Assets button '''
    nav_po.click_a_button(context, ASSETS_BUTTON_XPATH)
