#!/usr/bin/env python3

'''
Dragos Notifications Steps Module
'''

import time
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from selenium.common.exceptions import NoSuchElementException
from behave import then, when  # pylint: disable=no-name-in-module
import _support.page_objects.navigation_page_object as nav_po


NOTIFICATION_CONTAINER_ID = 'notifications-app-container '
NOTIFICATION_FILTER_HAMBURGER_ID = 'filters-dropdown-control__button'
FILTER_CRITERIA_TAB_NOTIFICATIONS_ID = 'sidebar__link-1'
FILTER_DROPDOWN_NOTIFICATIONS_XPATH = '//*[@id="search_criteria"]/div[2]/div[1]/div/div/input'
NOT_FILTER_DROPDOWN_XPATH = '//*[@id="__select-content"]/li[18]/span'
SELECT_OPERATION_DROPDOWN_NOTIF_XPATH = '//*[@id="search_criteria"]/div[2]/div[2]/div/div/input'
NOTIFICATION_FILTER_STATUS_IS_XPATH = '//*[@id="__select-content"]/li[1]/span'
NOTIFICATION_FILTER_VALUE_DROPDOWN_XPATH = '//*[@id="search_criteria"]/div[2]/div[3]/div/div/input'
NOTIFICATION_STATUS_UNREAD_XPATH = '//*[@id="__select-content"]/li[2]/span'
ADD_ATTRIBUTE_NOTIF_FILTER_BTN_XPATH = '//*[@id="search_criteria"]/div[3]/div/button'
NOTIF_FILTER_SANCE_RELOAD_BTN_XPATH = ('//*[@id="filters-dropdown-control__container"]/div/'
                                       'div[2]/div[2]/div/button[2]/span')


@then('I am on the notifications page')
def on_the_notifications_page(context):
    ''' On the notifications page '''
    try:
        WebDriverWait(context.browser, 20).until(EC.visibility_of_element_located((By.CLASS_NAME,
                                                                                   NOTIFICATION_CONTAINER_ID)))
        nav_po.verify_page_class(context, NOTIFICATION_CONTAINER_ID)
        nav_po.assertion_passed(context)
    except (TimeoutException, NoSuchElementException, AssertionError):
        nav_po.assertion_failed(context)


@when('I filter for unread notifications')
def filter_for_unread_notifications(context):
    ''' Filter for unread notifications '''
    try:
        notification_filter_hamburger = context.browser.find_element(By.ID, NOTIFICATION_FILTER_HAMBURGER_ID)

        # open filter menu
        notification_filter_hamburger.click()

        filter_criteria_tab_notifications = context.browser.find_element(By.ID, FILTER_CRITERIA_TAB_NOTIFICATIONS_ID)

        # click filter tab
        filter_criteria_tab_notifications.click()
        time.sleep(2)

        # click the filter dropdown
        filter_dropdown_notifications = context.browser.find_element(By.XPATH, FILTER_DROPDOWN_NOTIFICATIONS_XPATH)
        filter_dropdown_notifications.click()
        time.sleep(2)

        # select 'status' from the dropdown
        status_from_not_filter_dropdown = context.browser.find_element_by_xpath(NOT_FILTER_DROPDOWN_XPATH)
        status_from_not_filter_dropdown.click()
        time.sleep(2)

        # select the operation dropdown
        notif_filter_select_op_dropdown = context.browser.find_element(By.XPATH, SELECT_OPERATION_DROPDOWN_NOTIF_XPATH)
        notif_filter_select_op_dropdown.click()
        time.sleep(2)

        # select is
        notification_filter_status_is = context.browser.find_element(By.XPATH, NOTIFICATION_FILTER_STATUS_IS_XPATH)
        notification_filter_status_is.click()
        time.sleep(2)

        # select value dropdown
        notif_filter_value_dropdown = context.browser.find_element(By.XPATH, NOTIFICATION_FILTER_VALUE_DROPDOWN_XPATH)
        notif_filter_value_dropdown.click()

        # select unread
        notification_status_unread = context.browser.find_element(By.XPATH, NOTIFICATION_STATUS_UNREAD_XPATH)
        notification_status_unread.click()
        time.sleep(2)

        # add attribute
        add_attribute_notif_filter_btn = context.browser.find_element(By.XPATH, ADD_ATTRIBUTE_NOTIF_FILTER_BTN_XPATH)
        add_attribute_notif_filter_btn.click()
        time.sleep(2)

        # click save and reload
        notif_filter_sance_and_reload_btn = context.browser.find_element(By.XPATH, NOTIF_FILTER_SANCE_RELOAD_BTN_XPATH)
        notif_filter_sance_and_reload_btn.click()
        nav_po.assertion_passed(context)
    except (NoSuchElementException, AssertionError):
        nav_po.assertion_failed(context)
