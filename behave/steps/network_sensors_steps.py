#!/usr/bin/env python3

'''
Dragos Network Sensors Steps Module
'''

from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from selenium.common.exceptions import NoSuchElementException
from behave import then  # pylint: disable=no-name-in-module
import _support.page_objects.login_page as login_po


NETWORK_SENSOR_ID = 'sensormanager-app-container'


@then('the network sensor tab loads with the network sensor table')
def network_sensor_table_loads(context):
    ''' Network sensor table loads '''
    try:
        WebDriverWait(context.browser, 60).until(EC.visibility_of_element_located((By.ID, NETWORK_SENSOR_ID)))
        network_sensor_table = context.browser.find_element(By.ID, NETWORK_SENSOR_ID)
        assert network_sensor_table.is_displayed()
        login_po.assertion_passed(context)
    except (TimeoutException, NoSuchElementException, AssertionError):
        login_po.assertion_failed(context)
