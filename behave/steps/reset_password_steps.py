#!/usr/bin/env python3

'''
Dragos Reset Password Steps Module
'''

import time
from selenium.common.exceptions import NoSuchElementException
from behave import then, when  # pylint: disable=no-name-in-module
import _support.page_objects.login_page as login_po
import _support.page_objects.reset_password_page_object as rpass_po


@when('I click the reset password link')
def click_reset_password_link(context):
    ''' Click the reset password link '''
    try:
        time.sleep(1)
        reset_password_link = rpass_po.get_reset_password_link(context)
        reset_password_link.click()
        rpass_po.assertion_passed(context)
    except (NoSuchElementException, AssertionError):
        rpass_po.assertion_failed(context)


@then('the user is navigated to the reset password page')
def taken_to_reset_password_page(context):
    ''' Taken to the reset password page '''
    try:
        time.sleep(1)
        old_password_field = rpass_po.get_old_password(context)
        assert old_password_field.is_displayed()
        rpass_po.assertion_passed(context)
    except (NoSuchElementException, AssertionError):
        rpass_po.assertion_failed(context)


@when('I enter the users valid credentials for a password reset')
def enter_valid_credentials(context):
    ''' Enter valid credentials '''
    try:
        time.sleep(1)
        username_field = rpass_po.get_password_reset_username_field(context)
        old_password_field = rpass_po.get_old_password(context)
        login_po.send_valid_username(username_field)
        login_po.send_valid_password(old_password_field)
        time.sleep(1)
        rpass_po.assertion_passed(context)
    except (NoSuchElementException, AssertionError):
        rpass_po.assertion_failed(context)


@when('I enter the current password as a new password')
def enter_current_password(context):
    ''' Enter current password '''
    try:
        new_password_field = rpass_po.get_new_password(context)
        conf_new_password_field = rpass_po.get_new_password(context)
        login_po.send_valid_password(new_password_field)
        login_po.send_valid_password(conf_new_password_field)
        rpass_po.assertion_passed(context)
    except (NoSuchElementException, AssertionError):
        rpass_po.assertion_failed(context)


@when('I enter password as the new password')
def enter_new_password(context):
    ''' Enter new password '''
    try:
        new_password_field = rpass_po.get_new_password(context)
        conf_new_password_field = rpass_po.get_new_password(context)
        rpass_po.send_new_reset_password(new_password_field, 'password')
        rpass_po.send_new_reset_password(conf_new_password_field, 'password')
        rpass_po.assertion_passed(context)
    except (NoSuchElementException, AssertionError):
        rpass_po.assertion_failed(context)


@when('I click reset password')
def click_reset_password(context):
    ''' Click reset password '''
    try:
        reset_password_button = rpass_po.get_reset_password_button(context)
        reset_password_button.click()
        rpass_po.assertion_passed(context)
    except (NoSuchElementException, AssertionError):
        rpass_po.assertion_failed(context)


@then('I receive a password reset error message')
def password_reset_error_message(context):
    ''' Receive password reset error message '''
    try:
        time.sleep(.5)
        error_message = rpass_po.get_password_reset_error(context)
        assert error_message.is_displayed()
        rpass_po.assertion_passed(context)
    except (NoSuchElementException, AssertionError):
        rpass_po.assertion_failed(context)
