#!/usr/bin/env python3

'''
Dragos Data Table Steps Module
'''

from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from selenium.common.exceptions import NoSuchElementException
from behave import when, then  # pylint: disable=no-name-in-module
import _support.page_objects.data_landing_page_object as dl_po


QFD_TITLE_XPATH = '//*[@id="root"]/div/div/div/div/div[4]/div/div/div/div/div[1]/div/div[2]/div[1]/a/div'
TASKING_MANAGER_TITLE_XPATH = ('//*[@id="root"]/div/div/div/div/div[4]/div/div/div/div/div[3]/div/'
                               'div[2]/div[1]/a/div')
FILES_TITLE_XPATH = '//*[@id="root"]/div/div/div/div/div[4]/div/div/div/div/div[2]/div/div[2]/div[1]/a/div'
QFD_LAUNCH_BUTTON_XPATH = '//*[@id="root"]/div/div/div/div/div[4]/div/div/div/div/div[1]/div/div[2]/div[2]/button'
QFD_CONTAINER_ID = 'qfds-summary-component'
TASKING_MANAGER_LAUNCH_BUTTON_XPATH = ('//*[@id="root"]/div/div/div/div/div[4]/div/div/div/div/div[3]/'
                                       'div/div[2]/div[2]/button')
FILES_LAUNCH_BUTTON_XPATH = '//*[@id="root"]/div/div/div/div/div[4]/div/div/div/div/div[2]/div/div[2]/div[2]/button'
TASKING_MANAGER_CONTAINER_ID = 'tasking-manager'
FILES_CONTAINER_XPATH = '//*[@id="root"]/div/div/div/div/div[4]/div/div'


@then('the correct data modules appear')
def data_modules_appear(context):
    ''' Data modules appear '''
    try:
        qfd_title = context.browser.find_element(By.XPATH, QFD_TITLE_XPATH).text
        tasking_manager_title = context.browser.find_element(By.XPATH, TASKING_MANAGER_TITLE_XPATH).text
        files_title = context.browser.find_element(By.XPATH, FILES_TITLE_XPATH).text
        assert qfd_title == 'Query-Focused Datasets'
        assert tasking_manager_title == 'Tasking Manager'
        assert files_title == 'Files'
        dl_po.assertion_passed(context)
    except (NoSuchElementException, AssertionError):
        dl_po.assertion_failed(context)


@when('I click the qfd launch button')
def click_qfd_launch_button(context):
    ''' Click QFD launch button '''
    try:
        qfd_launch_button = dl_po.get_qfd_launch_button(context)
        qfd_launch_button.click()
        dl_po.assertion_passed(context)
    except (NoSuchElementException, AssertionError):
        dl_po.assertion_failed(context)


@when('I click the tasking manager launch button')
def click_tasking_mgr_launch_button(context):
    ''' Click tasking manager launch button '''
    try:
        tasking_manager_launch_button = context.browser.find_element(By.XPATH, TASKING_MANAGER_LAUNCH_BUTTON_XPATH)
        tasking_manager_launch_button.click()
        dl_po.assertion_passed(context)
    except (NoSuchElementException, AssertionError):
        dl_po.assertion_failed(context)


@when('I click the files launch button')
def click_files_launch_button(context):
    ''' Click files launch button '''
    try:
        files_launch_button = context.browser.find_element(By.XPATH, FILES_LAUNCH_BUTTON_XPATH)
        files_launch_button.click()
        dl_po.assertion_passed(context)
    except (NoSuchElementException, AssertionError):
        dl_po.assertion_failed(context)


@then('I am on the QFD page')
def on_qfd_page(context):
    ''' On the QFD page '''
    try:
        WebDriverWait(context.browser, 60).until(EC.presence_of_element_located((By.ID, QFD_CONTAINER_ID)))
        assert context.browser.find_element_by_id(QFD_CONTAINER_ID)
        dl_po.assertion_passed(context)
    except (TimeoutException, NoSuchElementException, AssertionError):
        dl_po.assertion_failed(context)


@then('I am on the tasking manager page')
def on_tasking_mgr_page(context):
    ''' On tasking manager page '''
    try:
        WebDriverWait(context.browser, 60).until(EC.presence_of_element_located((By.ID, TASKING_MANAGER_CONTAINER_ID)))
        assert context.browser.find_element_by_id(TASKING_MANAGER_CONTAINER_ID)
        dl_po.assertion_passed(context)
    except (TimeoutException, NoSuchElementException, AssertionError):
        dl_po.assertion_failed(context)


@then('I am on the files page')
def on_files_page(context):
    ''' On files page '''
    try:
        WebDriverWait(context.browser, 60).until(EC.presence_of_element_located((By.XPATH, FILES_CONTAINER_XPATH)))
        assert context.browser.find_element_by_xpath(FILES_CONTAINER_XPATH)
        dl_po.assertion_passed(context)
    except (NoSuchElementException, AssertionError):
        dl_po.assertion_failed(context)
