#!/usr/bin/env python3

'''
Dragos Login Steps Module
'''

import time
from selenium.common.exceptions import NoSuchElementException
from behave import given, when, then  # pylint: disable=no-name-in-module
import _support.page_objects.login_page as login_po


LICENSE_INPUT_CHECKBOX = '//*[@id="login__accept-eula"]'
USER_LICENSE_LINK_XPATH = '//*[@id="external-form"]/div[5]/div[2]/span'
LICENSE_POPUP_ID = 'modal__eula__content'


@given('I am on the login page')
def on_login_page(context):
    ''' On the login page '''
    try:
        login_po.nav_to_platform(context)
    except (NoSuchElementException, AssertionError):
        login_po.assertion_failed(context)


@when('I type the username "(.*)"')
def type_username(context, username):
    ''' Enter the username '''
    try:
        username_field = login_po.find_username_field(context)
        username_field.send_keys(username)
        login_po.assertion_passed(context)
    except NoSuchElementException:
        login_po.assertion_failed(context)


@when('I type the password "(.*)"')
def type_password(context, password):
    ''' Enter the password '''
    try:
        password_field = login_po.find_password_field(context)
        password_field.send_keys(password)
        login_po.assertion_passed(context)
    except NoSuchElementException:
        login_po.assertion_failed(context)


@when('I click log in')
def click_log_in(context):
    ''' Click log in '''
    try:
        login_button = login_po.find_login_button(context)
        login_button.click()
        login_po.assertion_passed(context)
    except NoSuchElementException:
        login_po.assertion_failed(context)


@then('I see an error message')
def see_error_message(context):
    ''' Verify error message '''
    try:
        assert context.browser.find_element_by_class_name('form-error')
        login_po.assertion_passed(context)
    except NoSuchElementException:
        login_po.assertion_failed(context)


@when('I enter valid credentials')
def enter_valid_creds(context):
    ''' Enter valid credentials '''
    login_po.enter_valid_credentials(context)


@then('I am logged in')
def then_logged_in(context):
    ''' Verify successful login '''
    login_po.verify_logged_into_platform(context)


@given('I am logged in')
def given_am_logged_in(context):
    ''' Already logged in '''
    login_po.nav_to_platform(context)
    try:
        login_po.login_with_valid_credentials(context)
    except NoSuchElementException:
        login_po.assertion_failed(context)


@then('the password is masked')
def password_is_masked(context):
    ''' Verify password is masked '''
    password_field = login_po.find_password_field(context)
    field_type = login_po.get_field_type(password_field)
    try:
        assert field_type == "password"
        login_po.assertion_passed(context)
    except AssertionError:
        login_po.assertion_failed(context)


@when('I click show password')
def click_show_password(context):
    ''' Click show password button '''
    try:
        show_password_button = login_po.find_show_password_button(context)
        show_password_button.click()
        login_po.assertion_passed(context)
    except NoSuchElementException:
        login_po.assertion_failed(context)


@then('the password is not masked')
def pw_not_masked(context):
    ''' Verify password is not masked '''
    password_field = login_po.find_password_field(context)
    field_type = login_po.get_field_type(password_field)
    try:
        assert field_type == "text"
        login_po.assertion_passed(context)
    except AssertionError:
        login_po.assertion_failed(context)


@given('I log back in')
def log_back_in(context):
    ''' Log back in '''
    login_po.nav_to_platform(context)
    try:
        login_po.login_with_valid_credentials(context)
        login_po.assertion_passed(context)
    except NoSuchElementException:
        login_po.assertion_failed(context)


@when('I click the license checkbox')
def click_license_checkbox(context):
    ''' Click the license checkbox '''
    time.sleep(2)
    try:
        license_checkbox = login_po.find_license_checkbox(context)
        license_checkbox.click()
        time.sleep(2)
        login_po.assertion_passed(context)
    except NoSuchElementException:
        login_po.assertion_failed(context)


@when('I click on the user license link')
def click_user_license_link(context):
    ''' Verify the user license link '''
    try:
        user_license_link = context.browser.find_element_by_xpath(USER_LICENSE_LINK_XPATH)
        user_license_link.click()
        login_po.assertion_passed(context)
    except NoSuchElementException:
        login_po.assertion_failed(context)


@then('the checkbox is checked')
def checkbox_is_checked(context):
    ''' Verify license checkbox is checked '''
    try:
        assert context.browser.find_element_by_xpath(LICENSE_INPUT_CHECKBOX).is_selected()
        login_po.assertion_passed(context)
    except NoSuchElementException:
        login_po.assertion_failed(context)


@then('the checkbox is not checked')
def checkbox_not_checked(context):
    ''' Verify license checkbox is not checked '''
    time.sleep(1)
    try:
        assert not context.browser.find_element_by_xpath(LICENSE_INPUT_CHECKBOX).is_selected()
        login_po.assertion_passed(context)
    except NoSuchElementException:
        login_po.assertion_failed(context)


@then('the user license appears')
def user_license_appears(context):
    ''' Verify user license appears '''
    time.sleep(1)
    try:
        assert context.browser.find_element_by_id(LICENSE_POPUP_ID).is_displayed()
        login_po.assertion_passed(context)
    except NoSuchElementException:
        login_po.assertion_failed(context)


@then('the login button is not clickable')
def login_button_not_clickable(context):
    ''' Verify login button is not clickable '''
    try:
        assert not login_po.get_login_button(context).is_enabled()
        login_po.assertion_passed(context)
    except NoSuchElementException:
        login_po.assertion_failed(context)


@then('the login button is clickable')
def login_button_clickable(context):
    ''' Verify login button is clickable '''
    try:
        assert login_po.get_login_button(context).is_enabled()
        login_po.assertion_passed(context)
    except NoSuchElementException:
        login_po.assertion_failed(context)


@then('I agree to the user license')
def agree_to_license(context):
    ''' Verify can agree to user license '''
    try:
        user_license_checkbox = login_po.get_user_license_checkbox(context)
        user_license_checkbox.click()
    except NoSuchElementException:
        login_po.assertion_failed(context)
