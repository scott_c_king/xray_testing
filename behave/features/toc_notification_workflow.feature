Feature: The TOC nnotification workflow. This is an end-to-end test that is purposefully a run on.
  Workflow documented here: https://dragosics-my.sharepoint.com/:w:/g/personal/hrymel_dragos_com/EYLAXDfe0CJGl6UlWb6jfIYBhA4iirArLn9CzxnQWVpbNA?e=O7MMcT
  Worked with Marc S. to come up with it

  @PLATDEV-5024
## should I break this up into multiple scenarios?
  Scenario: Go through the notification workflow
    Given I am on the detections dashboard
    When I click a notification
    Then the notification popup has the correct fields
    When I click the "create a case" button
    Then the "Create a Case" module appears
    When I enter valid information
    And I click the submit button to create a case
    Then I am on the test cases journal
    And the test case has the information entered during creation
    When I click the journal placeholder
    Then the journal editor appears
    When I type information in the journal
     Then the entry appears in the journal
     When I attach code in the journal entry
     Then the age doesn't break
