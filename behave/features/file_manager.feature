Feature: File Manager functionality

    @PLATDEV-2705
    Scenario: The FM page loads with the appropriate buttons and containers
      Given I am on the file manager page
      Then the fm page has export, back, and column filter buttons

    @PLATDEV-2706
    Scenario: The export popup appears
      Given I am on the file manager page
      When I click the fm export button
      Then the fm export popup appears

    @PLATDEV-2707
    Scenario: The column picker popup appears
      Given I am on the file manager page
      When I click the fm column picker
      Then the fm column picker appears

    @PLATDEV-2600
    Scenario: The 'back' button returns me to the data page
      Given I am on the file manager page
      When I click the fm back button
      Then the page class "list_container" exists