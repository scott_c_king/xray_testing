Feature: Reset password

  @PLATDEV-2670
  Scenario: Reset Password link navigates the user to the reset password page
    Given I am on the login page
    When I click the reset password link
    Then the user is navigated to the reset password page

  @PLATDEV-2421
  Scenario:  Cannot reset password with previously used password
    Given I am on the login page
    When I click the reset password link
    When I enter the users valid credentials for a password reset
    When I enter the current password as a new password
    When I click reset password
    Then I receive a password reset error message

  @PLATDEV-2332
  Scenario: Cannot reset password with a weak password
    Given I am on the login page
    When I click the reset password link
    When I enter the users valid credentials for a password reset
    When I enter password as the new password
    When I click reset password
    Then I receive a password reset error message