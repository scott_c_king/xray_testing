Feature: Test the top nav bar, refered to as the Main Menu

  @PLATDEV-2322
  Scenario: Clicking set homepage star changes the star logo
    Given I am logged in
    When I click Notifications
    Then the page class "notifications-app-container" exists
    When I click the star
    Then the star is colored in
    When I close the browser

  @PLATDEV-2672
  Scenario: Clicking an active star, changes it back to an unfilled star
    Given I am logged in
    When I click Notifications
    Then the page class "notifications-app-container" exists
    When I click the star
    Then the star is colored in
    When I click the star
    Then the star is unfilled


  @PLATDEV-2673
  Scenario: Set homepage shows up as default on relogging in
    Given I am logged in
    When I click Notifications
    Then the page class "notifications-app-container" exists
    When I click the star
    Then the star is colored in
    When I click Reports
    When I logout
    Given I log back in
    Then the page class "notifications-app-container" exists
    When I close the browser

  @PLATDEV-2326
  Scenario: I logout
    Given I am logged in
    When I click Notifications
    When I logout
    Then I am logged out

  @PLATDEV-2321
  Scenario: Clicking fullscreen makes the window fullscreen
    Given I am logged in
    When I click Notifications
    And I make the window fullscreen
    Then the window is fullscreen