Feature: Content page

  @PLATDEV-2056
  Scenario: The content page loads
    Given I am on the content page
    Then the content page loads

  @PLATDEV-2598
  Scenario: The two tabs are present
    Given I am on the content page
    Then the detections and characterization tabs are present
#
#
  @PLATDEV-2711
  Scenario: The correct buttons and items are present (Group By, Dragos Toggle button,
            User Analytics button, Play button, Pause button, and '+ Analytic' button.)
    Given I am on the content page
    Then the correct content-detections items are present

  @PLATDEV-2710
  Scenario: The create new analytic popup appears
    Given I am on the content page
    When I click the + ANALYTIC button
    Then the create new analytic popup appears

 @PLATDEV-2708
  Scenario: When I click on the characterization tab, the page changes
    Given I am on the content page
    When I click the characterization tab
    Then I am on the content characterization tab
#

  @PLATDEV-2709
  Scenario: The correct buttons and items are present (Group By, Dragos Toggle button,
            User Analytics button, Play button, Pause button, and '+ Analytic' button.)
    Given I am on the content page
    When I click the characterization tab
    Then the correct character-detections items are present

  @PLATDEV-5008
  Scenario: Clicking char + ANALYTIC button opens popup
    Given I am on the content page
    When I click the characterization tab
    When I click the Char + ANALYTIC button
    Then the create new char analytic popup appears
