Feature: Test the Data table in accordance to the smoke test

  @PLATDEV-2335
  Scenario: Data page loads
    Given I am on the Data page
    Then the page class "list_container" exists

  @PLATDEV-2601
  Scenario: The three modules appear (QFD, Tasking Manager, and Data Explorer)
    Given I am on the Data page
    Then the correct data modules appear

  @PLATDEV-2366
  Scenario:  When I click the QFD launch button, I am navigated to the QFD page
    Given I am on the Data page
    When I click the qfd launch button
    Then I am on the QFD page

  @PLATDEV-2371
  Scenario: When I click the tasking manager launch button, I am navigated to the TM page
    Given I am on the Data page
    When I click the tasking manager launch button
    Then I am on the tasking manager page

  @PLATDEV-5009
  Scenario: When I click the files launch button, I am navigated to the files page
    Given I am on the Data page
    When I click the files launch button
    Then I am on the files page

