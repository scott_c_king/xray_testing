# adding a user adds a user to the company ldap. Need to clean up and add a delete functionality.


Feature: Test creating users and editing users as an admin

  @PLATDEV-2599
  Scenario: Create user popup appears
    Given I am on the admin page
    When I open the create user popup
    Then the pop-up appears

   @PLATDEV-2341
  Scenario: Create a user (customer)
    Given I am on the admin page
    When I open the create user popup
    And I enter valid data
    Then I am on the user's profile


    #Scenario: Edit User
    #Scenraio: Cannot create user with invalid data
    #Scenario: # of users updates whe creating a user