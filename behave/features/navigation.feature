Feature: Navigating to different pages

  Scenario Outline: Navigate to pages as a logged in user
    Given I am logged in
    When I click "<button>"
    Then the page class "<class_name>" exists
    When I close the browser

    @PLATDEV-2329
    Examples:
        | button                                       | class_name                       |
        #admin
        | //*[@id="primary-nav__upper__icon-links__admin"]    | admin-app-container              |

    @PLATDEV-2338
    Examples:
        | button                                       | class_name                       |
        #sensors
        | //*[@id="primary-nav__upper__icon-links__sensors"]    | sensormanager-app-container      |

    @PLATDEV-2337
    Examples:
        | button                                       | class_name                       |
        #reports
        | //*[@id="primary-nav__upper__icon-links__reportmanager"]     | reports-container                |

    @PLATDEV-2336
    Examples:
        | button                                       | class_name                       |
        #baselines
        | //*[@id="primary-nav__upper__icon-links__baselinemanager"]     | baselines-app-container          |


    @PLATDEV-2055
    Examples:
        | button                                       | class_name                       |
        #content
        | //*[@id="primary-nav__upper__icon-links__contentmanager"]     | content-container                |


    @PLATDEV-5016
    Examples:
        | button                                       | class_name                       |
        #notifications
        | //*[@id="primary-nav__upper__icon-links__notificationmanager"]     | notifications-app-container      |

    @PLATDEV-5017
    Examples:
        | button                                       | class_name                       |
        #data
        | //*[@id="primary-nav__upper__icon-links__dataexplorer"]     | app-item-name                     |


    @PLATDEV-2334
    Examples:
        | button                                       | class_name                       |
        #assets
        | //*[@id="primary-nav__upper__icon-links__assetexplorer"]    | assetexplorer-app-container       |


    @PLATDEV-5018
    Examples:
        | button                                       | class_name                       |
        #map
        | //*[@id="primary-nav__upper__icon-links__map"]     | MapTaskbar                       |

  @PLATDEV-2340
  Scenario: I click the slide page button, what is shown changes
    Given I am logged in
    Then I am on the "right" tab
    # click to make sure the nav bar is on the left
    When I click "//*[@id="primary-nav"]/div[2]/div/div[3]/div[1]"
    # click to move the nav bar to the left
    When I click "//*[@id="primary-nav"]/div[2]/div/div[3]/div[2]/i"
    Then I am on the "left" tab
