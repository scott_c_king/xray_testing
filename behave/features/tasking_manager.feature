Feature: Tasking Manager functionality

  @PLATDEV-5025
  Scenario: The Tasking Manager page loads
    Given I am on the tasking manager page
    Then I am on the tasking manager page

    @PLATDEV-2679
    Scenario: The TM page loads with the appropriate buttons and containers
      Given I am on the tasking manager page
      Then the tm page has export, back, and column filter buttons

    @PLATDEV-2680
    Scenario: The export popup appears
      Given I am on the tasking manager page
      When I click the tm export button
      Then the tm export popup appears

    @PLATDEV-2681
    Scenario: The column picker popup appears
      Given I am on the tasking manager page
      When I click the tm column picker
      Then the tm column picker appears

    @PLATDEV-2372
    Scenario: The 'back' button returns me to the data page
      Given I am on the tasking manager page
      When I click the tm back button
      Then I am on the tasking manager page