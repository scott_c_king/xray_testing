Feature: Test the notifications manager
#
  @PLATDEV-5019
  Scenario: The notification page loads
    When I navigate to the notifications page
    Then I am on the notifications page

    @PLATDEV-5020
  Scenario: A user can set the filter on the notification manager to show only unread notifications
    When I navigate to the notifications page
    Then I am on the notifications page
    When I filter for unread notifications
    Then the status == unread filter is visible