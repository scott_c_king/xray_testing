Feature: Assets page

  @PLATDEV-2597
  Scenario: Column headers appears on the assets page
    Given I am logged in
    When I click on the asset button
    Then the page class "assetexplorer-app-container" exists
    Then I select a network
    Then the correct Asset column header are present

  @PLATDEV-2675
  Scenario: Zones tab has the appropriate column headers
    Given I am on the assets page
    When I click on the zones tab
    Then the correct Zone column headers are present

  @PLATDEV-5005
  Scenario: Click 'create new zone' and create new zone popup appears
    Given I am on the assets page
    When I click on the zones tab
    And I click on create a new zone
    Then the create a new zone popup appears

  @PLATDEV-2364
  Scenario: Click the refresh button on zones page
    Given I am on the assets page
    When I click on the zones tab
    Then I click the zones refresh button
    Then the correct Zone column headers are present

  @PLATDEV-2676
  Scenario: Click the refresh button on assets page
    Given I am on the assets page
    Then I select a network
    Then I click the asset refresh button
    Then the correct Asset column header are present

  @PLATDEV-2677
  Scenario: Export popup appears
    Given I am on the assets page
    When I click on the zones tab
    And I click the zone export button
    Then the export popup appears

##
#  #the following two tests are unfinished - don't run
##  Scenario: Export the zone data as CSV
##    Given I am on the assets page
##    When I click on the zones tab
##    And I click the export button
##    Then the export popup appears
##  #  When I click export a csv
##  #  Then a file is downloaded
##
##  Scenario: Export the zone data as TSV
##    Given I am on the assets page
##    When I click on the zones tab
##    And I click the export button
##    Then the export popup appears
###    When I click export a tsv
###    Then a file is downloaded

#
#  #additional tests
#  # create zone
#  # create asset