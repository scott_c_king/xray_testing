Feature: If on the Apps page, it is possible to navigate to all apps


  Scenario Outline: Navigate to page
    Given I am logged in
    When I click "<button>"
    Then the page class "<class_name>" exists

    Examples: button and class names
    | button        | class_name                 |
    #PLATDEV-2329
    # admin
    | //*[@id="root"]/div/div/div/div[4]/div/div/div/div/div[9]/div/div[2]/div[2]/button | admin-app-container           |
    #interactive mao
    | //*[@id="root"]/div/div/div/div[4]/div/div/div/div/div[1]/div/div[2]/div[2]/button | map-loader                    |
    #asset explorer
    | //*[@id="root"]/div/div/div/div[4]/div/div/div/div/div[2]/div/div[2]/div[2]/button | assetexplorer-table-container |
    #data explorer
    | //*[@id="root"]/div/div/div/div[4]/div/div/div/div/div[3]/div/div[2]/div[2]/button | apps-list-module                |
    #notification manager
    | //*[@id="root"]/div/div/div/div[4]/div/div/div/div/div[4]/div/div[2]/div[2]/button | notifications-app-container   |
    #content manager
    | //*[@id="root"]/div/div/div/div[4]/div/div/div/div/div[5]/div/div[2]/div[2]/button  | analytic-tabs |
    # baseline manager
    | //*[@id="root"]/div/div/div/div[4]/div/div/div/div/div[6]/div/div[2]/div[2]/button  | baselines-app-container       |
    #report manager
    | //*[@id="root"]/div/div/div/div[4]/div/div/div/div/div[7]/div/div[2]/div[2]/button  | reports-container             |
    # sensor manager
    | //*[@id="root"]/div/div/div/div[4]/div/div/div/div/div[8]/div/div[2]/div[2]/button  | midpointmanager-table-container |
    # log search and visualizer
    #| //*[@id="root"]/div/div/div/div[4]/div/div/div/div/div[10]/div/div[2]/div[2]/button | kibana-body                      |
