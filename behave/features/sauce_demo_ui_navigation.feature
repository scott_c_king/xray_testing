Feature: Sauce Demo Web Navigation

  Scenario: Sauce Demo Web Navigation
    Given I am on the login page
    When I enter valid credentials
    Then I agree to the user license
    When I click log in
    Then I am logged in

    When I click "//*[@id="primary-nav__upper__icon-links__map"]"
    Then the page class "map-loader" exists

    When I click "//*[@id="primary-nav__upper__icon-links__assetexplorer"]"
    Then the page class "assetexplorer-table-container" exists 

    When I click "//*[@id="primary-nav__upper__icon-links__dataexplorer"]"
    Then the page class "apps-list-module" exists

    When I click "//*[@id="primary-nav__upper__icon-links__notificationmanager"]"
    Then the page class "notifications-app-container" exists

    When I click "//*[@id="primary-nav__upper__icon-links__contentmanager"]"
    Then the page class "analytic-tabs" exists

    When I click "//*[@id="primary-nav__upper__icon-links__baselinemanager"]"
    Then the page class "baselines-app-container" exists

    When I click "//*[@id="primary-nav__upper__icon-links__reportmanager"]"
    Then the page class "reports-container" exists

    When I click "//*[@id="primary-nav__upper__icon-links__sensors"]"
    Then the page class "sensormanager-app-container" exists

    When I click "//*[@id="primary-nav__upper__icon-links__admin"]"
    Then the page class "admin-app-container" exists

    When I click "//*[@id="primary-nav__upper__icon-links__apps"]"
    Then the page class "apps-list-module" exists

    When I logout
    Then I am logged out
