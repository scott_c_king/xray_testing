Feature: Login to the platform

  @PLATDEV-5004
  Scenario Outline: Unable to login with invalid credentials
    Given I am on the login page
    When I type the username "<username>"
    When I type the password "<password>"
    Then the password is masked
    Then I agree to the user license
    When I click log in
    Then I see an error message

    Examples: Username and Passwords
      | username       | password         |
      | aa             | aaaaa            |
      | admin          | admin            |


  @PLATDEV-2327
  Scenario: The password is masked by default
    Given I am on the login page
    When I type the password "gobblygook"
    Then the password is masked

  @PLATDEV-2328
  Scenario: Does 'show password' unmask password characters
    Given I am on the login page
    When I type the password "gobblygook"
    Then the password is masked
    When I click show password
    Then the password is not masked
    When I click show password
    Then the password is masked
#    When I close the browser

  @PLATDEV-2318
  Scenario: Login with valid credentials
    Given I am on the login page
    When I enter valid credentials
    Then I agree to the user license
    When I click log in
    Then I am logged in

  @PLATDEV-2329
  Scenario: Click the user license checkbox toggles the checkbox
    Given I am on the login page
    When I click the license checkbox
    Then the checkbox is checked
    When I click the license checkbox
    Then the checkbox is not checked

  @PLATDEV-2330
  Scenario: Clicking the license link shows the user license
    Given I am on the login page
    When I click on the user license link
    Then the user license appears

  @PLATDEV-2419
  Scenario: Cannot log in without the user license box checked
    Given I am on the login page
    When I type the username "username"
    When I type the password "gobblygook"
    Then the login button is not clickable
    When I click the license checkbox
    Then the login button is clickable
