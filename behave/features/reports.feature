Feature: Reports page

  @PLATDEV-2714
  Scenario: The reports page has the correct columns
    Given I am on the reports page
    Then the reports table has the correct columns

  @PLATDEV-2715
  Scenario: The page has the correct buttons
    Given I am on the reports page
    Then the correct buttons are there

  @PLATDEV-2716
  Scenario: Clicking create report causes popup to appear
    Given I am on the reports page
    When I click create report
    Then the create new report popup appears

  @PLATDEV-2717
  Scenario: Clicking export reports button causes popup to appear
    Given I am on the reports page
    When I click the export report button
    Then the export report popup appears

  @PLATDEV-2718
  Scenario: Clicking the reports column picker shows the column picker popup
    Given I am on the reports page
    When I click the report column picker button
    Then the report column picker popup appears