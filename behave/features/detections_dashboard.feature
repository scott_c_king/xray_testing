Feature: Check the detection dashboard to make sure that all items are present

  @PLATDEV-5010
  Scenario: All modules are present
    Given I am logged in
    When I click "//*[@id="primary-nav"]/div[2]/div/div[3]/div[2]/i"
    Then I am on the "left" tab
    Then The "modeling" module is present
    Then The "threat" module is present
    Then The "config" module is present
    Then The "indicator" module is present

  @PLATDEV-5011
  Scenario: All modules have correct titles
    Given I am logged in
    When I click the "right" full-slide arrow
    Then I am on the "left" tab
    Then The modules have correct titles

  @PLATDEV-5011
   Scenario Outline: Clicking the module errors, when notifications are shown, changes the graph type
     Given I am logged in
     When I click the "right" full-slide arrow
     Then I am on the "left" tab
     When I click "<arrow_type>"
     Then the "<graph>" bar graph shows

     Examples: Arrow xpaths and Related Graph Xpaths
     | arrow_type                                                                                        | graph                                                                                                |
     # right modeling arrow
     | //*[@id="root"]/div/div/div/div/div[2]/div/div/div[2]/div[1]/div/div/div[1]/div[2]/div/div[3]/div | //*[@id="root"]/div/div/div/div/div[2]/div/div/div[2]/div[1]/div/div/div[1]/div[2]/div/div[2]/div[1] |
     # threat behavior
     | //*[@id="root"]/div/div/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[1]/div[2]/div/div[3]/div  | //*[@id="root"]/div/div/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[1]/div[2]/div/div[2]/div[1] |
     # config
     | //*[@id="root"]/div/div/div/div/div[2]/div/div/div[2]/div[3]/div/div/div[1]/div[2]/div/div[3]/div  | //*[@id="root"]/div/div/div/div/div[2]/div/div/div[2]/div[3]/div/div/div[1]/div[2]/div/div[2]/div[1]  |
     # indicator
     |  //*[@id="root"]/div/div/div/div/div[2]/div/div/div[2]/div[4]/div/div/div[1]/div[2]/div/div[3]/div  | //*[@id="root"]/div/div/div/div/div[2]/div/div/div[2]/div[4]/div/div/div[1]/div[2]/div/div[2]/div[1] |


    @PLATDEV-5012
    Scenario Outline: Module on the detection dashboard should have pagination
              Pages contain 20 list items
      Given I am logged in
      When I click the "right" full-slide arrow
      Then I am on the "left" tab
      Then the "<notification_list>" has 20 items per page

      Examples: Notification list xpaths
      | notification_list |
      # threat behavior
      | .detection-dashboard-main-panel .detection-dashboard-sub-panel:nth-child(2) ul.collection li |
      #config
      | .detection-dashboard-main-panel .detection-dashboard-sub-panel:nth-child(3) ul.collection li |
      # indicator
      # indicator dashboard is empty right now
     # | .detection-dashboard-main-panel .detection-dashboard-sub-panel:nth-child(4) ul.collection li |


  @PLATDEV-5013
  Scenario: When I click on a notification, a pop up appears
    Given I am logged in
    When I click "//*[@id="primary-nav"]/div[2]/div/div[3]/div[2]/i"
    Then I am on the "left" tab
    When I click a notification
    Then the pop-up appears

  @PLATDEV-5014
  Scenario: Modeling: Hovering over the information icon shows the hover text
    Given I am on the detections dashboard
    When I hover over the modeling information icon
    Then the modeling information text appears

  @PLATDEV-2740
  Scenario: Threat Behavior: Hovering over the information icon shows the hover text
    Given I am on the detections dashboard
    When I hover over the threat behavior information icon
    Then the threat behavior information text appears

  @PLATDEV-2554
  Scenario: Config: Hovering over the information icon shows the hover text
    Given I am on the detections dashboard
    When I hover over the configuration information icon
    Then the configuration information text appears

  @PLATDEV-2563
  Scenario: Indicator: Hovering over the information icon shows the hover text
    Given I am on the detections dashboard
    When I hover over the indicator information icon
    Then the indicator information text appears

  @PLATDEV-2423
  Scenario: The modeling notification menu appears
    Given I am on the detections dashboard
    When I click the modeling detections notification ...
    Then the modeling detections submenu appears

  @PLATDEV-2548
  Scenario: The threat behavior notification menu appears
    Given I am on the detections dashboard
    When I click the modeling detections notification ...
    Then the threat behavior detections submenu appears

   @PLATDEV-2664
   Scenario: Able to change the 'From' date on the Detections Dashboard datepicker
      Given I am on the detections dashboard
      When I set the date to include all of January 2020
      Then the detections dashboard date range changes
#
  @PLATDEV-5015
  Scenario: DN3P modeling notification has correct fields populated
    Given I am logged in
    When I click "//*[@id="primary-nav"]/div[2]/div/div[3]/div[2]/i"
    When I set the date to include all of January 2020
    ##Verify the alerts contain what I think
    When I click a DN3P detections modeling alert
    Then the detections modeling alert popup appears
    Then the detections modeling alert popup has the expected fields populated









     # Test cases for the detection page
     # sort By - each item
    # sort direction
    # show read/unread
    # min/max severity -per module, per severityl
    # do the number shown on indicator match the number of events listed
    # date filter
    # info hover - per module
    # ellipse click -per module
    # ellipse ->hide notification per module
    # refresh notification per module
    # notification -> pagination - per module
    # open each notification type, and verify correct fields are present