Feature: QFD functionality

  @PLATDEV-5021
  Scenario: The QFD page loads
    Given I am on the QFD page
    Then I am on the QFD page

    @PLATDEV-5022
    Scenario: The QFD page loads with the appropriate buttons and containers
      Given I am on the QFD page
      Then the page has export, back, and column filter buttons

    @PLATDEV-2678
    Scenario: The export popup appears
      Given I am on the QFD page
      When I click the qfd export button
      Then the qfd export popup appears

    @PLATDEV-2713
    Scenario:  The export popup's toggle works
      Given I am on the QFD page
      When I click the qfd export button
      When I click the csv/tsv toggle
      Then the toggle toggles

    @PLATDEV-5023
    Scenario: The export module changes the available columns to export
      Given I am on the QFD page
      When I click the qfd export button
      When a qfd export column name checkbox is checked
      And I click the export column name checkbox
      Then the checkbox is not checked

    @PLATDEV-2370
    Scenario: The column picker popup appears
      Given I am on the QFD page
      When I click the QFD column picker
      Then the qfd column picker appears

    @PLATDEV-2367
    Scenario: The 'back' button returns me to the data page
      Given I am on the QFD page
      When I click the QFD back button
      Then I am on the QFD page

