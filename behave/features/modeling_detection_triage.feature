Feature: Follow a real TOC worklfow for triaging a modeling detection alert
  #video followed for refrence found here: https://www.dropbox.com/home/Company%20Share/Platform%20Feedback%20Videos%20and%20Screenshots/Detection%20Triage%201.5.2?preview=Platform+Modeling+Detection+triage.mp4
  #run API calls to populate some DNS Zone Transfer Alerts before running these tests

  @PLATDEV-2621
  Scenario: Triage a modeling detection alert
    Given I am logged in
    When I click "//*[@id="primary-nav"]/div[2]/div/div[3]/div[2]/i"
    #The Detections modeling graph loads and is visible with data
    #Then the detections modeling graph is visible
    When I set the group by to ungrouped
#    Then the detections page shows the ungrouped graphs
    When I set the date to include all of January 2020
    Then the detections dashboard date range changes
    #Then the detections modeling list of incident is populated
    ##Verify the severity bar graph
    When I click the right modeling graph arrow
    Then the severity bar graph is visible
    ##Verify the alerts contain what I think
    #filter by severity
    When I click a DN3P detections modeling alert
    Then the detections modeling alert popup appears
    Then the detections modeling alert popup has the expected fields populated
    #Go to notifications page to verify some asset stuff
    When I navigate to the notifications page
    Then I am on the notifications page
    When I filter for unread notifications
    #need to make this assertion stronger
    Then the status == unread filter is visible
#    # Find any alerts that are different by filtering out the most common/low risk
#    # since not able to filter by modeling, find number that are common/low risk and subtract
    When I set the notification filter to show all that are 'Detected by' 'DNS Zone Transfer Request Detected'
#    Then I compare the number of notifications from from the NM and the DM
#    #inspect notifications from the nm to see if all alerts are from the same asset
#    #this workflow works best if all or most of notifications in modeling are by the same asset
#    When I hover over asset id
#    Then I see the assets id numbers involved in the notification
#    When I filter for one of the notification ids
#    When I compare the number of visible assets with the unfiltered number of assets
#    # Go to detections and view detection that caused notification to see more information
#    When I navigated to the Content Manager
#    When I search for the notification detected by type (DNS Zone Tranfer Request)
#    Then the content page shows the detection
#    Then the detection shows the detection details
#    # Open playbook from Detections page
    When I click "//*[@id="primary-nav"]/div[2]/div/div[3]/div[2]/i"
#    When I click a detections modeling alert (DNS Zone Transfer Request)
#    Then the detections modeling alert popup appears
#    When I click the playbook link
#    Then I am navigated to the DNS Zone Transfer Playbook
#    Then the DNS Zone Transfer Playbook has visible details and steps
#    Then the playbook steps have both titles and step details
#    #Return to the Detections page and
    When I click "//*[@id="primary-nav"]/div[2]/div/div[3]/div[2]/i"
    When I set the date to include all of January 2020
#    Then the alerts load with the same data set as before
#    When I click a detections modeling alert
#    Then the detections modeling alert popup appears
#    #Get information about the first asset from the alert by pivoting to the asset explorer
#    When I pivot from a detections model alert detail page to the asset explorer from the first asset ID
#    Then the asset explorer loads with the pivoted asset visible
#    When I expand the asset
#    Then the asset details are visible
#    When I hover over the protocol bar graph
#    Then I see the count and type of protocol
#    #Get information about the second asset from the alert by pivoting to the asset explorer
    When I click "//*[@id="primary-nav"]/div[2]/div/div[3]/div[2]/i"
    When I set the date to include all of January 2020
    Then the alerts load with the same data set as before
    When I click a detections modeling alert
    Then the detections modeling alert popup appears
    When I pivot from a detections model alert detail page to the asset explorer from the first asset ID
    Then the asset explorer loads with the pivoted asset visible
    When I expand the asset
    Then the asset details are visible
    When I hover over the protocol bar graph
    Then I see the count and type of protocol
    # Would Marc do all of this before opening an incident?
    # How did he know they were internal transfers?
    # Is triaging an alert just finding it in notification and decising if an incidnet should be opened?
    # Or does it include going through the playbook workflow?


#pivoting from detecrted by on the detections alert to content page
  #view notification from content
  #view error logs for the content
  #subnet ip need to always visible



