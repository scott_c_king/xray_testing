#!/bin/bash

# Verify the required local environment variables are set
echo "INFO: Verifying local environment variables are set..."
if [ -z ${SAUCE_USERNAME} ] ; then echo "ERROR: SAUCE_USERNAME is not set. Try sourcing your local environment file." ; exit 1 ; else echo "INFO: SAUCE_USERNAME is set." ; fi
if [ -z ${SAUCE_ACCESS_KEY} ] ; then echo "ERROR: SAUCE_ACCESS_KEY is not set. Try sourcing your local environment file." ; exit 1 ; else echo "INFO: SAUCE_ACCESS_KEY is set." ; fi
if [ -z ${TUNNEL_IDENTIFIER} ] ; then echo "ERROR: TUNNEL_IDENTIFIER is not set. Try sourcing your local environment file." ; exit 1 ; else echo "INFO: TUNNEL_IDENTIFIER is set." ; fi
echo "INFO: Local environment OK. Starting Sauce Connect tunnel now..."

# Daemonize Sauce Labs Connect Proxy
${SAUCE_CONNECT_PATH} -u ${SAUCE_USERNAME} -k ${SAUCE_ACCESS_KEY} -i ${TUNNEL_IDENTIFIER} &
