#!/bin/bash

# Verify the required local environment variables are set
echo "INFO: Verifying local environment variables are set..."
if [ -z ${SAUCE_USERNAME} ] ; then echo "ERROR: SAUCE_USERNAME is not set. Try sourcing your local environment file." ; exit 1 ; else echo "INFO: SAUCE_USERNAME is set." ; fi
if [ -z ${TUNNEL_IDENTIFIER} ] ; then echo "ERROR: TUNNEL_IDENTIFIER is not set. Try sourcing your local environment file." ; exit 1 ; else echo "INFO: TUNNEL_IDENTIFIER is set." ; fi
echo "INFO: Local environment OK. Stopping Sauce Connect tunnel now..."

# Stop Sauce Labs Connect Proxy
kill `ps -ef | grep ${SAUCE_USERNAME} | grep ${TUNNEL_IDENTIFIER} | awk 'NR==1 { print$2 }'`
