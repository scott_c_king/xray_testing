#!/usr/bin/env python3

'''
Platform UI XRay Publisher

NOTE: This is intended to be run from within Jenkins only!
'''

__author__ = "Scott C. King"
__version__ = "0.1.0"
__license__ = "MIT"


import os
import requests


CLIENT_ID = os.environ['XRAY_API_CLIENT_ID']
CLIENT_SECRET = os.environ['XRAY_API_CLIENT_SECRET']
# https://xray.cloud.xpand-it.com/api/v1/import/execution/junit?projectKey=PLATDEV&testPlanKey=PLATDEV-5599
XRAY_BASE_URL = 'https://xray.cloud.xpand-it.com'
XRAY_API_AUTHENTICATE = "/api/v1/authenticate"
XRAY_API_IMPORT_JUNIT = "/api/v1/import/execution/junit"
JIRA_PROJECT_KEY = 'PLATDEV'
JIRA_PLAN_KEY = 'PLATDEV-5599'
XRAY_PROJECT_KEYS = '?projectKey={}&testPlanKey={}'


def get_bearer_token(url, client_id, client_secret):
    ''' Given client ID and secret, returns the bearer token '''
    url = url + XRAY_API_AUTHENTICATE
    payload = {"client_id": client_id, "client_secret": client_secret}
    headers = {"Content-Type": "application/json"}
    resp = requests.post(url, payload, headers)
    token = resp.json()

    return token


def build_payload_list():
    ''' Return a list of JUNIT XML files in current working directory. '''
    junit_xmls = []
    files = os.listdir(os.getcwd())
    for file in files:
        if file.startswith('TESTS'):
            junit_xmls.append(file.strip())
    print('JUnit Files Found: ', junit_xmls)
    return junit_xmls


def publish_xray_results(results_url, payload, bearer_token):
    ''' Publish the JUNIT results '''

    headers = {'Content-Type': 'text/xml', 'Authorization': f'Bearer {bearer_token}'}
    resp = requests.post(results_url, data=payload, headers=headers)
    if not resp.ok:
        print(f'ERROR: Failed to publish XRay results!\nResponse Test: {resp.text}\nPayload: {payload}')
    else:
        print('INFO: Results published successfully.')


def main():
    '''
    - Authenticate to get token
    - Build a list of JUNIT XML files in CWD
    - Import payload (JUNIT XML test results)
    - POST the JUNIT results to XRay
    '''

    bearer_token = get_bearer_token(XRAY_BASE_URL, CLIENT_ID, CLIENT_SECRET)
    print('Bearer Token:', bearer_token)

    # Get a list of the JUNIT XML files in the workspace
    junit_payload = build_payload_list()

    # Iterate on payload list, publish results
    results_url = XRAY_BASE_URL + XRAY_API_IMPORT_JUNIT + XRAY_PROJECT_KEYS.format(JIRA_PROJECT_KEY, JIRA_PLAN_KEY)
    print(f'INFO: Results URL [\'{results_url}\']')
    for junit_xml in junit_payload:
        with open(junit_xml, 'r') as xml_file:
            payload = xml_file.read()
        publish_xray_results(results_url, payload, bearer_token)


if __name__ == "__main__":
    main()
