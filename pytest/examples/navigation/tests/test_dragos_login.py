#!/usr/bin/env python3

'''
Pytest: Sauce Performance Testing
'''


from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC


def test_login_general_navigation(pt_webdriver):
    ''' Log in '''
    pt_webdriver.get('https://platform-dev10.dragos.services/#/login')
    pt_webdriver.find_element_by_id('username').send_keys('admin')
    pt_webdriver.find_element_by_id('password').send_keys('Dr@gosSyst3m')
    pt_webdriver.find_element_by_xpath('//*[@id="external-form"]/div[5]/div[1]').click()
    pt_webdriver.find_element_by_css_selector('#external-form > button').click()
    WebDriverWait(pt_webdriver, 10).until(EC.presence_of_element_located((By.CLASS_NAME, 'menu-controls')))
    print('INFO: Successfully Logged In...')
    # We're logged in...

    WebDriverWait(pt_webdriver, 10).until(EC.presence_of_element_located((By.ID, 'primary-nav__upper__icon-links__map')))
    pt_webdriver.find_element_by_id('primary-nav__upper__icon-links__map').click()
    WebDriverWait(pt_webdriver, 10).until(EC.visibility_of_element_located((By.CLASS_NAME, 'map-loader')))
    print('INFO: Map page loaded...')

    WebDriverWait(pt_webdriver, 10).until(EC.presence_of_element_located((By.ID, 'primary-nav__upper__icon-links__assetexplorer')))
    pt_webdriver.find_element_by_id('primary-nav__upper__icon-links__assetexplorer').click()
    WebDriverWait(pt_webdriver, 10).until(EC.visibility_of_element_located((By.CLASS_NAME, 'assetexplorer-table-container')))
    print('INFO: Asset Explorer page loaded...')

    WebDriverWait(pt_webdriver, 10).until(EC.presence_of_element_located((By.ID, 'primary-nav__upper__icon-links__dataexplorer')))
    pt_webdriver.find_element_by_id('primary-nav__upper__icon-links__dataexplorer').click()
    WebDriverWait(pt_webdriver, 10).until(EC.visibility_of_element_located((By.CLASS_NAME, 'apps-list-module')))
    print('INFO: Data Explorer page loaded...')

    # Etc ...
