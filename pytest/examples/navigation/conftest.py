#!/usr/bin/env python3

'''
Pytest Sample Fixture - Please do NOT use this file AS-IS.

This is an example of how to create a Pytest fixture for executing tests
with Selenium using either the Alpha (4.x) or Stable (3.x) releases.

Replace the appropriate values in the ALPHA_PERF and STANDARD profiles.
Set the correct job_name depending on profile used.
'''


import pytest
from selenium import webdriver
from selenium.common.exceptions import WebDriverException


import urllib3
urllib3.disable_warnings()

# The Alpha profile requires Selenium 4.x (currently in Alpha)
# The Stable profile (non-performance enabled) will run on Alpha (4.x) or Stable (3.x)
# The Platform-UI test container currently uses Selenium 3.14.

ALPHA_PERF_PROFILE = [{'platformName': 'macOS 10.15',
                       'browserName': 'chrome',
                       'browserVersion': 'latest',
                       'sauce:options': {'username': 'dragos_qa',
                                         'accessKey': '674552f5-1157-427c-ae6f-4ffd07cf523a',
                                         'tunnelIdentifier': 'scking-proxy-tunnel',
                                         'name': 'Sauce Performance - Pytest Navigation Selenium 4 Alpha',
                                         'build': 'Sauce Performance - Pytest Navigation Selenium 4 Alpha',
                                         'screenResolution': '1600x1200',
                                         'seleniumVersion': '4.0.0a5',
                                         'extendedDebugging': 'True',
                                         'capturePerformance': 'True'}}]

STABLE_PROFILE = [{'platform': 'macOS 10.15',
                   'browserName': 'chrome',
                   'version': 'latest',
                   'username': 'dragos_qa',
                   'accessKey': '674552f5-1157-427c-ae6f-4ffd07cf523a',
                   'tunnelIdentifier': 'scking-proxy-tunnel',
                   'name': 'Pytest Navigation Selenium 3.x (Stable)',
                   'build': 'Pytest Navigation Selenium 3.x (Stable)',
                   'screenResolution': '1600x1200'}]


@pytest.fixture(params=ALPHA_PERF_PROFILE)
def pt_webdriver(request):
    ''' Webdriver Fixture '''
    selenium_endpoint = 'https://ondemand.saucelabs.com:443/wd/hub'
    capabilities = dict()
    capabilities.update(request.param)
    browser = webdriver.Remote(command_executor=selenium_endpoint,
                               desired_capabilities=capabilities,
                               keep_alive=True)

    # STANDARD_PROFILE:
    # job_name = request.param['name']
    # ALPHA_PROFILE:
    job_name = request.param['sauce:options']['name']
    if browser is not None:
        print("SauceOnDemandSessionID={} job-name={}".format(browser.session_id, job_name))
    else:
        raise WebDriverException("ERROR: Web driver not created!")

    yield browser

    sauce_result = "failed" if request.node.rep_call.failed else "passed"
    browser.execute_script("sauce:job-result={}".format(sauce_result))
    browser.quit()


@pytest.hookimpl(hookwrapper=True, tryfirst=True)
def pytest_runtest_makereport(item):
    ''' Sauce Labs Reporting '''
    outcome = yield
    rep = outcome.get_result()
    setattr(item, "rep_" + rep.when, rep)
