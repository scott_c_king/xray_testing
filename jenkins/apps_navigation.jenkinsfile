// Jenkinsfile (Declarative Pipeline Syntax)

pipeline {
    agent { dockerfile true }

    parameters {
        choice(name: 'SSHOST', choices: ['https://platform-dev10.dragos.services/#/login', 'https://platform-dev03.dragos.services/#/login', 'https://platform.dragos.services/#/login'], description: 'Select the target Sitestore...')
        choice(name: 'SAUCE_BROWSERS', choices: ['chrome_latest_catalina_1600x1200.json', 'top_4_browsers_latest.json', 'top_4_browsers_last_two_releases.json'], description: 'Select OS/Browser combination...')
        choice(name: 'TEST_PLAN', choices: ['PLATDEV-5599'], description: 'JIRA test plan for publishing results...')
    }

    stages {

        stage('Apps Navigation Feature') {
            steps {
                sauce('8af7393e-a22a-4530-bfa5-b036c02df73e') {
                    sauceconnect(useGeneratedTunnelIdentifier: true, useLatestSauceConnect: true, verboseLogging: true) {
                        sh "env | sort > sauce_labs_job_env.txt"
                        sh "python3 behave/behave_runner.py behave/browser_configs/${SAUCE_BROWSERS} behave/features/apps_navigation.feature jenkins"
                    }
                }
            }
        }

        stage('Run Sauce Labs Publisher') {
            steps {
                saucePublisher()
            }
        }

        stage('XRay: Import JUnit XML') {
            steps {
                withCredentials([usernamePassword(credentialsId: 'haley_xray_platform_ui_creds', usernameVariable: 'XRAY_API_CLIENT_ID', passwordVariable: 'XRAY_API_CLIENT_SECRET')]) {
                sh "python3 utils/xray_publisher.py"
                }
            }
        }

    }

    post {
        always {
            // We are keeping the Behave log (*.log) and JUnit XML (*.xml)
            archiveArtifacts artifacts: '*.log, *.xml', fingerprint: true
            echo 'End of Line'
            cleanWs()
        }
    }

}
