#!/usr/bin/env bash

# ---------------------------------------------------------------------------
# SSHOST
# Target Sitestore for your local UI testing/development

export SSHOST="https://platform-dev10.dragos.services/#/login"

# ---------------------------------------------------------------------------
# SAUCE_USERNAME
# dragos_qa is a valid account - please replace with your Sauce Labs username

export SAUCE_USERNAME="dragos_qa"

# ---------------------------------------------------------------------------
# SAUCE_ACCESS_KEY
# This can be obtained by:
# 1. Log into Sauce dashboard: https://app.saucelabs.com/dashboard/builds
# 2. Click your username (top right corner)
# 3. Select User Settings
# 4. Access key is just below your user information
# 5. Copy/paste your key here
#
# The default key here is for the dragos_qa (Scott King) user:

export SAUCE_ACCESS_KEY="674552f5-1157-427c-ae6f-4ffd07cf523a"

# ---------------------------------------------------------------------------
# SAUCE_BUILD_NAME
# Jobs in the Sauce Labs dashboard are grouped by build. To organize your
# work in your dashboard, make this unique to you or your current task.
#
# Examples:
# scking_performance_development
# haley_sensors_some_new_sensor

export SAUCE_BUILD_NAME="Local Testing"

# ---------------------------------------------------------------------------
# TUNNEL_IDENTIFIER
# The Sauce Connect tunnel ID is a unique name for your tunnel instance.
# Do not share tunnels or use the Jenkins tunnel as they can be ephemeral.
#
# This environment variable is also used by the tunnel start/stop scripts:
# utils/sauce_connect/macos|linux/start_sauce_proxy.sh
# utils/sauce_connect/macos|linux/stop_sauce_proxy.sh
#
# Examples:
# scking_sauce_tunnel
# danny_sl_tunnel

export TUNNEL_IDENTIFIER="sc-proxy-tunnel"

# ---------------------------------------------------------------------------
# SAUCE_CONNECT_PATH
# The absolute local path to the sauce connect binary. Allows you to run the
# start/stop scripts without having to change into the local directory.
# Note: Rememeber to swap macos with linux if running in Docker!

export SAUCE_CONNECT_PATH="/abs_path_to/platform-ui-testing/utils/sauce_connect/macos/*/bin/sc"
